package com.chefmonster.android;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.text.DecimalFormat;
import java.util.ArrayList;

// adapter class fro custom menu list
class AdapterMenuList extends BaseAdapter {

	private Activity activity;
	public RelativeLayout checkout;
	public ImageLoader imageLoader;
	private TextView mTxtAmountAdapter;
	private TextView txtItems;
	int ItemCount=0;

	public AdapterMenuList(Activity act,TextView txtAmount,RelativeLayout checkout,TextView txtItems) {
		this.activity = act;
		imageLoader = new ImageLoader(act);
		this.mTxtAmountAdapter = txtAmount  ;
		this.txtItems=txtItems;
		this.checkout=checkout;
	}

	public int getCount() {
		// TODO Auto-generated method stub
		return ActivityMenuList.Menu_ID.size();
	}

	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	public View getView(final int position, View convertView, final ViewGroup parent) {
		// TODO Auto-generated method stub
		final ViewHolder holder;

		if(convertView == null){
			LayoutInflater inflater = (LayoutInflater) activity
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflater.inflate(R.layout.menu_list_item, null);
			holder = new ViewHolder();

			convertView.setTag(holder);
		}else{
			holder = (ViewHolder) convertView.getTag();
		}

		holder.txtText = (TextView) convertView.findViewById(R.id.txtText);
		holder.txtSubText = (TextView) convertView.findViewById(R.id.txtSubText);
		holder.imgThumb = (ImageView) convertView.findViewById(R.id.imgThumb);
		holder.txtweigthingms=(TextView) convertView.findViewById(R.id.txtweightgms);
		holder.txtStockQty=(TextView) convertView.findViewById(R.id.txtStockQty);
		holder.txtfoodtype=(TextView) convertView.findViewById(R.id.txtfoodtype);
		holder.txtDeliverytime=(TextView) convertView.findViewById(R.id.txtDtime);

		holder.txtText.setText(ActivityMenuList.Menu_name.get(position));
		holder.txtSubText.setText(ActivityMenuList.Menu_price.get(position)+" "+ActivityMenuList.Currency);
		holder.txtDeliverytime.setText(ActivityMenuList.Menu_delivery_time.get(position));
		holder.txtfoodtype.setText(ActivityMenuList.Menu_food_type.get(position));
		holder.txtStockQty.setText(ActivityMenuList.Menu_stock_qty.get(position));
		holder.txtweigthingms.setText(ActivityMenuList.Menu_weigth_grms.get(position));

		holder.txtQty=(TextView) convertView.findViewById(R.id.txtQty);
		holder.btninc=(Button) convertView.findViewById(R.id.btninc);
		holder.btndrc=(Button) convertView.findViewById(R.id.btndrc);
		imageLoader.DisplayImage(Constant.AdminPageURL+ActivityMenuList.Menu_image.get(position), holder.imgThumb);
		holder.btninc.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				int qty = Integer.parseInt(holder.txtQty.getText().toString());
				if (qty < 0) {

					ItemCount--;
				} else {
					ItemCount++;

					checkout.setVisibility(View.VISIBLE);
					qty++;
					double price=ActivityMenuList.Menu_price.get(position);
					double changedPrice=price*qty;
					//holder.txtSubText.setText(changedPrice+" "+ActivityMenuList.Currency);
					holder.txtQty.setText(String.valueOf(qty));

					DBHelper db=new DBHelper(parent.getContext());
					db.openDataBase();
					long menuid=ActivityMenuList.Menu_ID.get(position);
					double menuprice=ActivityMenuList.Menu_price.get(position);
					String menuname=ActivityMenuList.Menu_name.get(position);
					if(db.isDataExist(menuid)){
						db.updateData(menuid, qty, (menuprice*qty));
					}else{
						db.addData(menuid, menuname, qty, (menuprice*qty));
					}

					double currentCartPrice = Double.parseDouble(mTxtAmountAdapter.getText().toString());

					double ChangedPrice=currentCartPrice+menuprice;

					//mTxtAmountAdapter.setText(String.valueOf(ChangedPrice));
					db.close();
				}
				txtItems.setText(String.valueOf(ItemCount));
				mTxtAmountAdapter.setText(String.valueOf(getDataFromDatabase(parent.getContext())));
			}
		});
		holder.btndrc.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				int qty=Integer.parseInt(holder.txtQty.getText().toString());
				qty--;

				if(qty>0)
					ItemCount--;

				if(qty<=0)
				{


					double price=ActivityMenuList.Menu_price.get(position);
				//	holder.txtSubText.setText(price+" "+ActivityMenuList.Currency);
					holder.txtQty.setText("0");

					long menuid=ActivityMenuList.Menu_ID.get(position);
					DBHelper db=new DBHelper(parent.getContext());
					db.openDataBase();
					db.deleteData(menuid);
					db.close();

					double currentCartPrice = Double.parseDouble(mTxtAmountAdapter.getText().toString());

					if(currentCartPrice>0)
					{
						double ChangedPrice=currentCartPrice-price;
						//mTxtAmountAdapter.setText(String.valueOf(ChangedPrice));
					}

				}
				else
				{
					ItemCount--;
					if(qty==1)
					{
						double price=ActivityMenuList.Menu_price.get(position);
						holder.txtQty.setText("0");
						//holder.txtSubText.setText(price+" "+ActivityMenuList.Currency);
						DBHelper db=new DBHelper(parent.getContext());
						db.openDataBase();
						long menuid=ActivityMenuList.Menu_ID.get(position);
						double menuprice=ActivityMenuList.Menu_price.get(position);
						String menuname=ActivityMenuList.Menu_name.get(position);
						if(db.isDataExist(menuid)){
							db.updateData(menuid, qty, (menuprice * qty));
						}else{
							db.addData(menuid, menuname, qty, (menuprice*qty));
						}
						double currentCartPrice = Double.parseDouble(mTxtAmountAdapter.getText().toString());

						double ChangedPrice=currentCartPrice-price;

						//mTxtAmountAdapter.setText(String.valueOf(ChangedPrice));
						db.close();
					}
					else
					{

						double price=ActivityMenuList.Menu_price.get(position);
						double changedPrice=price*qty;
						//holder.txtSubText.setText(changedPrice+" "+ActivityMenuList.Currency);
						holder.txtQty.setText(String.valueOf(qty));
						DBHelper db=new DBHelper(parent.getContext());
						db.openDataBase();
						long menuid=ActivityMenuList.Menu_ID.get(position);
						double menuprice=ActivityMenuList.Menu_price.get(position);
						String menuname=ActivityMenuList.Menu_name.get(position);
						if(db.isDataExist(menuid)){
							db.updateData(menuid, qty, (menuprice * qty));
						}else{
							db.addData(menuid, menuname, qty, (menuprice*qty));
						}
						double currentCartPrice = Double.parseDouble(mTxtAmountAdapter.getText().toString());

						double ChangedPrice=currentCartPrice-price;

						//	mTxtAmountAdapter.setText(String.valueOf(ChangedPrice));
						db.close();
					}
				}
				mTxtAmountAdapter.setText(String.valueOf(getDataFromDatabase(parent.getContext())));
				txtItems.setText(String.valueOf(ItemCount));

			}
		});
		return convertView;
	}

	public double getDataFromDatabase(Context ctx){

		DecimalFormat formatData = new DecimalFormat("#.##");
		ArrayList<ArrayList<Object>> data;
		ArrayList<Double> Sub_total_price = new ArrayList<Double>();
		double Total_price = 0;
		DBHelper dbhelper=new DBHelper(ctx);
		dbhelper.openDataBase();
		data = dbhelper.getAllData();

		// store data to arraylist variables
		for(int i=0;i<data.size();i++){
			ArrayList<Object> row = data.get(i);
			Sub_total_price.add(Double.parseDouble(formatData.format(Double.parseDouble(row.get(3).toString()))));
			Total_price +=Sub_total_price.get(i);
		}

		// count total order

		//Total_price -= (Total_price * (Tax/100));
		Total_price = Double.parseDouble(formatData.format(Total_price));
		dbhelper.close();
		return Total_price;
	}
	static class ViewHolder {
		TextView txtText, txtSubText,txtQty,txtStockQty,txtDeliverytime,txtweigthingms,txtfoodtype;
		ImageView imgThumb;
		Button btninc,btndrc;
		RatingBar rb;
	}


}