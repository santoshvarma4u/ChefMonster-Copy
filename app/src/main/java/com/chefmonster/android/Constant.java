package com.chefmonster.android;

import java.io.InputStream;
import java.io.OutputStream;

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class Constant {

	public static String AdminPageURL = "http://128.199.173.98/admin/";
	public static String CategoryAPI = "http://128.199.173.98/api/get-all-category-data.php";
	public static String MenuAPI = "http://128.199.173.98/api/get-menu-data-by-category-id.php";
	public static String TaxCurrencyAPI = "http://128.199.173.98/api/get-tax-and-currency.php";
	public static String MenuDetailAPI = "http://128.199.173.98/api/get-menu-detail.php";
	public static String SendDataAPI = "http://128.199.173.98/api/add-reservation.php";

	// change this access similar with accesskey in admin panel for security reason
	public static String AccessKey = "12345";
	
	// database path configuration
	public  static String DBPath = "/data/data/com.chefmonster.android/databases/";
	
	// method to check internet connection
	public static boolean isNetworkAvailable(Activity activity) {
		ConnectivityManager connectivity = (ConnectivityManager) activity
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		if (connectivity == null) {
			return false;
		} else {
			NetworkInfo[] info = connectivity.getAllNetworkInfo();
			if (info != null) {
				for (int i = 0; i < info.length; i++) {
					if (info[i].getState() == NetworkInfo.State.CONNECTED) {
						return true;
					}
				}
			}
		}
		return false;
	}
	
	// method to handle images from server
	public static void CopyStream(InputStream is, OutputStream os)
    {
        final int buffer_size=1024;
        try
        {
            byte[] bytes=new byte[buffer_size];
            for(;;)
            {
              int count=is.read(bytes, 0, buffer_size);
              if(count==-1)
                  break;
              os.write(bytes, 0, count);
            }
        }
        catch(Exception ex){}
    }

}
