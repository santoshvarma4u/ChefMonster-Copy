package com.chefmonster.android;

import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.PagerAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TabHost;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.chefmonster.android.Deals.DealsDeliveryFragment;
import com.chefmonster.android.Deals.DealsDeliveryTab;
import com.chefmonster.android.Deals.DealsPickupFragment;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpConnectionParams;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URLEncoder;
import java.text.DecimalFormat;
import java.util.ArrayList;

import cn.yangbingqiang.android.parallaxviewpager.ParallaxViewPager;

public class ActivityDeals extends Fragment {

    ListView listMenu;
    ProgressBar prgLoading;
    //TextView txtTitle;
    EditText edtKeyword;
    ImageButton btnSearch;
    TextView txtAlert,txtAmountCart;
    Button btnCartAdapter;
    TabHost tHost;
    // declare static variable to store tax and currency symbol
    static double Tax;
    static String Currency;

    // declare adapter object to create custom menu list
    AdapterDeals mla;

    // create arraylist variables to store data from server
    static ArrayList<Long> Menu_ID = new ArrayList<Long>();
    static ArrayList<String> Menu_name = new ArrayList<String>();
    static ArrayList<Double> Menu_price = new ArrayList<Double>();
    static ArrayList<String> Menu_image = new ArrayList<String>();
    static ArrayList<String> Menu_stock_qty = new ArrayList<String>();
    static ArrayList<String> Menu_food_type = new ArrayList<String>();
    static ArrayList<String> Menu_delivery = new ArrayList<String>();
    static ArrayList<String> Menu_weigth_grms = new ArrayList<String>();
    static ArrayList<String> Menu_serves = new ArrayList<String>();
    static ArrayList<String> Menu_delivery_time = new ArrayList<String>();


    String MenuAPI;
    String TaxCurrencyAPI;
    int IOConnect = 0;
    long Category_ID;
    String Category_name;
    String Keyword;
    private ParallaxViewPager mParallaxViewPager;
    // create price format
    DecimalFormat formatData = new DecimalFormat("#.##");
    private String[] mImages = new String[]{
            "http://128.199.173.98/images/homeslider.jpg"

    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.deal_list, container, false);
        prgLoading = (ProgressBar) v.findViewById(R.id.prgLoadingdeals);
        listMenu = (ListView) v.findViewById(R.id.listMenudeals);
        edtKeyword = (EditText) v.findViewById(R.id.edtKeyworddeals);
        btnSearch = (ImageButton) v.findViewById(R.id.btnSearchdeals);
        txtAlert = (TextView) v.findViewById(R.id.txtAlertdeals);
        txtAmountCart=  (TextView) v.findViewById(R.id.txtCartPriceAdapterdeals);
        btnCartAdapter=(Button) v.findViewById(R.id.btnCartAdapterdeals);

        tHost = (TabHost) v.findViewById(R.id.tabHost);
        tHost.setup();
        TabHost.OnTabChangeListener tabChangeListener = new TabHost.OnTabChangeListener() {

            @Override
            public void onTabChanged(String tabId) {
            /*     FragmentManager fm=getFragmentManager();
                DealsDeliveryFragment dfv=(DealsDeliveryFragment)fm.findFragmentById(R.id.dealsdeliveryfragment);
                DealsPickupFragment dpf=(DealsPickupFragment)fm.findFragmentById(R.id.dealspickupfragment);*/


            }
        };
        TabHost.TabSpec tSpecDelivery = tHost.newTabSpec("Delivery");
        tSpecDelivery.setIndicator("Delivery");
        tSpecDelivery.setContent(new DealsDeliveryTab(getActivity().getApplicationContext()));
        tHost.addTab(tSpecDelivery);

        TabHost.TabSpec tSpecPickup = tHost.newTabSpec("Pickup");
        tSpecPickup.setIndicator("Pickup");
        tSpecPickup.setContent(new DealsDeliveryTab(getActivity().getApplicationContext()));
        tHost.addTab(tSpecPickup);


        mParallaxViewPager = (ParallaxViewPager) v.findViewById(R.id.viewpagerdeals);
        initViewPager();
        txtAmountCart.setText(String.valueOf(getDataFromDatabase(getActivity())));

        btnCartAdapter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent iMyOrder = new Intent(getActivity(), ActivityCart.class);
                startActivity(iMyOrder);
                getActivity().overridePendingTransition (R.anim.open_next, R.anim.close_next);
            }
        });
        // menu API url
        MenuAPI = Constant.MenuAPI+"?accesskey="+Constant.AccessKey+"&category_id=";
        // tax and currency API url
        TaxCurrencyAPI = Constant.TaxCurrencyAPI+"?accesskey="+Constant.AccessKey;

        // get category id and category name that sent from previous page
        Intent iGet = getActivity().getIntent();
        Category_ID = iGet.getLongExtra("category_id",0);
        //Category_name = iGet.getStringExtra("category_name");
        MenuAPI += "";

        mla = new AdapterDeals(getActivity(),txtAmountCart);

        // call asynctask class to request tax and currency data from server
        new getTaxCurrency().execute();

        // event listener to handle search button when clicked
        btnSearch.setOnClickListener(new View.OnClickListener() {

            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                // get keyword and send it to server
                try {
                    Keyword = URLEncoder.encode(edtKeyword.getText().toString(), "utf-8");
                } catch (UnsupportedEncodingException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                MenuAPI += "&keyword="+Keyword;
                IOConnect = 0;
                listMenu.invalidateViews();
                clearData();
                new getDataTask().execute();
            }
        });

        listMenu.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            public void onItemClick(AdapterView<?> arg0, View arg1, int position,
                                    long arg3) {
                // TODO Auto-generated method stub
                // go to menu detail page
               /* Intent iDetail = new Intent(getActivity(), ActivityMenuDetail.class);
                iDetail.putExtra("menu_id", Menu_ID.get(position));
                startActivity(iDetail);
                getActivity().overridePendingTransition(R.anim.open_next, R.anim.close_next);*/
            }
        });

        return v;
    }



    private void initViewPager() {
        PagerAdapter adapter = new PagerAdapter() {

            @Override
            public boolean isViewFromObject(View arg0, Object arg1) {
                return arg0 == arg1;
            }

            @Override
            public void destroyItem(ViewGroup container, int position,
                                    Object obj) {
                container.removeView((View) obj);
            }

            @Override
            public Object instantiateItem(ViewGroup container, int position) {
                View view = View.inflate(container.getContext(), R.layout.pager_item, null);
                ImageView imageView = (ImageView) view.findViewById(R.id.item_img);
                Glide.with(ActivityDeals.this).load(mImages[position % mImages.length]).into(imageView);
                container.addView(view, ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.MATCH_PARENT);
                return view;
            }

            @Override
            public int getCount() {
                return 40;
            }
        };
        mParallaxViewPager.setAdapter(adapter);
    }
    public double getDataFromDatabase(Context ctx){

        DecimalFormat formatData = new DecimalFormat("#.##");
        ArrayList<ArrayList<Object>> data;
        ArrayList<Double> Sub_total_price = new ArrayList<Double>();
        double Total_price = 0;
        DBHelper dbhelper=new DBHelper(ctx);
        dbhelper.openDataBase();
        data = dbhelper.getAllData();

        // store data to arraylist variables
        for(int i=0;i<data.size();i++){
            ArrayList<Object> row = data.get(i);
            Sub_total_price.add(Double.parseDouble(formatData.format(Double.parseDouble(row.get(3).toString()))));
            Total_price +=Sub_total_price.get(i);
        }

        // count total order

        //Total_price -= (Total_price * (Tax/100));
        Total_price = Double.parseDouble(formatData.format(Total_price));
        dbhelper.close();
        return Total_price;
    }


    // asynctask class to handle parsing json in background
    public class getTaxCurrency extends AsyncTask<Void, Void, Void> {

        // show progressbar first
        getTaxCurrency(){
            if(!prgLoading.isShown()){
                prgLoading.setVisibility(0);
                txtAlert.setVisibility(8);
            }
        }

        @Override
        protected Void doInBackground(Void... arg0) {
            // TODO Auto-generated method stub
            // parse json data from server in background
            parseJSONDataTax();
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            // TODO Auto-generated method stub
            // when finish parsing, hide progressbar
            prgLoading.setVisibility(8);
            // if internet connection and data available request menu data from server
            // otherwise, show alert text
            if((Currency != null) && IOConnect == 0){
                new getDataTask().execute();
            }else{
                txtAlert.setVisibility(0);
            }
        }
    }

    // method to parse json data from server
    public void parseJSONDataTax(){
        try {
            // request data from tax and currency API
            HttpClient client = new DefaultHttpClient();
            HttpConnectionParams.setConnectionTimeout(client.getParams(), 15000);
            HttpConnectionParams.setSoTimeout(client.getParams(), 15000);
            HttpUriRequest request = new HttpGet(TaxCurrencyAPI);
            HttpResponse response = client.execute(request);
            InputStream atomInputStream = response.getEntity().getContent();

            BufferedReader in = new BufferedReader(new InputStreamReader(atomInputStream));

            String line;
            String str = "";
            while ((line = in.readLine()) != null){
                str += line;
            }


            // parse json data and store into tax and currency variables
            JSONObject json = new JSONObject(str);
            JSONArray data = json.getJSONArray("data"); // this is the "items: [ ] part

            JSONObject object_tax = data.getJSONObject(0);
            JSONObject tax = object_tax.getJSONObject("tax_n_currency");

            Tax = Double.parseDouble(tax.getString("Value"));

            JSONObject object_currency = data.getJSONObject(1);
            JSONObject currency = object_currency.getJSONObject("tax_n_currency");

            Currency = currency.getString("Value");
        } catch (MalformedURLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            IOConnect = 1;
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    // clear arraylist variables before used
    void clearData(){
        Menu_ID.clear();
        Menu_name.clear();
        Menu_price.clear();
        Menu_image.clear();
        Menu_stock_qty.clear();
        Menu_delivery.clear();
        Menu_delivery_time.clear();
        Menu_food_type.clear();
        Menu_serves.clear();
        Menu_weigth_grms.clear();
    }

    // asynctask class to handle parsing json in background
    public class getDataTask extends AsyncTask<Void, Void, Void>{

        // show progressbar first
        getDataTask(){
            if(!prgLoading.isShown()){
                prgLoading.setVisibility(0);
                txtAlert.setVisibility(8);
            }
        }

        @Override
        protected Void doInBackground(Void... arg0) {
            // TODO Auto-generated method stub
            // parse json data from server in background
            parseJSONData();
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            // TODO Auto-generated method stub
            // when finish parsing, hide progressbar
            prgLoading.setVisibility(8);

            // if data available show data on list
            // otherwise, show alert text
            if(Menu_ID.size() > 0){
                listMenu.setVisibility(0);
                listMenu.setAdapter(mla);
            }else{
                txtAlert.setVisibility(0);
            }

        }
    }

    // method to parse json data from server
    public void parseJSONData(){

        clearData();

        try {
            // request data from menu API
            HttpClient client = new DefaultHttpClient();
            HttpConnectionParams.setConnectionTimeout(client.getParams(), 15000);
            HttpConnectionParams.setSoTimeout(client.getParams(), 15000);
            HttpUriRequest request = new HttpGet(MenuAPI);
            HttpResponse response = client.execute(request);
            InputStream atomInputStream = response.getEntity().getContent();

            BufferedReader in = new BufferedReader(new InputStreamReader(atomInputStream));

            String line;
            String str = "";
            while ((line = in.readLine()) != null){
                str += line;
            }

            // parse json data and store into arraylist variables
            JSONObject json = new JSONObject(str);
            JSONArray data = json.getJSONArray("data"); // this is the "items: [ ] part

            for (int i = 0; i < data.length(); i++) {
                JSONObject object = data.getJSONObject(i);

                JSONObject menu = object.getJSONObject("Menu");

                Log.e("Menu",menu.toString());

                Menu_ID.add(Long.parseLong(menu.getString("Menu_ID")));
                Menu_name.add(menu.getString("Menu_name"));
                Menu_price.add(Double.valueOf(formatData.format(menu.getDouble("Price"))));
                Menu_image.add(menu.getString("Menu_image"));
                Menu_stock_qty.add(menu.getString("Quantity"));
                Menu_delivery.add(menu.getString("delivery_type"));
                Menu_delivery_time.add(menu.getString("delivery_time"));
                Menu_food_type.add(menu.getString("type_of_food"));
                Menu_serves.add(menu.getString("no_of_serves"));
                Menu_weigth_grms.add(menu.getString("weight_in_grams"));
            }


        } catch (MalformedURLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }



    @Override
    public void onConfigurationChanged(final Configuration newConfig)
    {
        // Ignore orientation change to keep activity from restarting
        super.onConfigurationChanged(newConfig);
    }


}
