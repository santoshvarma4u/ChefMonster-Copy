package com.chefmonster.android;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpConnectionParams;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.database.SQLException;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import de.keyboardsurfer.android.widget.crouton.Crouton;
import de.keyboardsurfer.android.widget.crouton.Style;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.chefmonster.android.Login.SmartLoginBuilder;
import com.chefmonster.android.Login.SmartLoginConfig;
import com.chefmonster.android.Login.manager.UserSessionManager;
import com.chefmonster.android.Login.users.SmartFacebookUser;
import com.chefmonster.android.Login.users.SmartGoogleUser;
import com.chefmonster.android.Login.users.SmartUser;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.model.LatLng;

public class ActivityCart extends Activity {
	
	// declare view objects
//	ImageButton imgNavBack;
	ListView listOrder;
	ProgressBar prgLoading;
	TextView txtTotalLabel, txtTotal, txtAlert;
	Button btnClear, Checkout;
	RelativeLayout lytOrder;
	
	// declate dbhelper and adapter objects
	DBHelper dbhelper;
	AdapterCart mola;
	
	
	// declare static variables to store tax and currency data
	static double Tax;
	static String Currency;

	// declare arraylist variable to store data
	ArrayList<ArrayList<Object>> data;
	static ArrayList<Integer> Menu_ID = new ArrayList<Integer>();
	static ArrayList<String> Menu_name = new ArrayList<String>();
	static ArrayList<Integer> Quantity = new ArrayList<Integer>();
	static ArrayList<Double> Sub_total_price = new ArrayList<Double>();
	
	double Total_price;
	final int CLEAR_ALL_ORDER = 0;
	final int CLEAR_ONE_ORDER = 1;
	int FLAG;
	int ID;
	String TaxCurrencyAPI;
	int IOConnect = 0;

	Geocoder geocoder;
	List<Address> addresses;
	int PLACE_PICKER_REQUEST = 4879;

	// create price format
	DecimalFormat formatData = new DecimalFormat("#.##");

	SmartUser currentUser=null;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mycart);

		geocoder = new Geocoder(this, Locale.getDefault());

        ActionBar bar = getActionBar();
        bar.setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.header)));
        bar.setTitle("Order Detail");
        bar.setDisplayHomeAsUpEnabled(true);
        bar.setHomeButtonEnabled(true);

		final SmartLoginBuilder loginBuilder = new SmartLoginBuilder();




		// connect view objects with xml id
//        imgNavBack = (ImageButton) findViewById(R.id.imgNavBack);
        Checkout = (Button) findViewById(R.id.Checkout);
        prgLoading = (ProgressBar) findViewById(R.id.prgLoading);
        listOrder = (ListView) findViewById(R.id.listOrder);
        txtTotalLabel = (TextView) findViewById(R.id.txtTotalLabel);
        txtTotal = (TextView) findViewById(R.id.txtTotal);
        txtAlert = (TextView) findViewById(R.id.txtAlert);
        btnClear = (Button) findViewById(R.id.btnClear);
        lytOrder = (RelativeLayout) findViewById(R.id.lytOrder);
        
        // tax and currency API url
        TaxCurrencyAPI = Constant.TaxCurrencyAPI+"?accesskey="+Constant.AccessKey;
    	
        mola = new AdapterCart(this,txtTotal);
        dbhelper = new DBHelper(this);
        
        // open database
        try{
			dbhelper.openDataBase();
		}catch(SQLException sqle){
			throw sqle;
		}

        // call asynctask class to request tax and currency data from server
        new getTaxCurrency().execute();

        // event listener to handle clear button when clicked
		btnClear.setOnClickListener(new OnClickListener() {
			
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				// show confirmation dialog
				showClearDialog(CLEAR_ALL_ORDER, 1111);
			}
		});
		
        // event listener to handle list when clicked
		listOrder.setOnItemClickListener(new OnItemClickListener() {

			public void onItemClick(AdapterView<?> arg0, View arg1, int position,
					long arg3) {
				// show confirmation dialog
				System.out.println("in click");
				showClearDialog(CLEAR_ONE_ORDER, Menu_ID.get(position));
			}
		});
        
        // event listener to handle back button when clicked
//        imgNavBack.setOnClickListener(new OnClickListener() {
//			
//			public void onClick(View arg0) {
//				// TODO Auto-generated method stub
//				// close database and back to previous page
//				dbhelper.close();
//				finish();
//				overridePendingTransition(R.anim.open_main, R.anim.close_next);
//			}
//		});
        
        Checkout.setOnClickListener(new OnClickListener() {
			
			public void onClick(View arg0) {


				// TODO Auto-generated method stub
				// close database and back to previous page
				/*dbhelper.close();

				Intent iReservation = new Intent(ActivityCart.this, ActivityCheckout.class);
				startActivity(iReservation);
				overridePendingTransition(R.anim.open_next, R.anim.close_next);*/
				//currentUser = UserSessionManager.getCurrentUser(ActivityCart.this);


			if(didUserLoggedin())
			{
				dbhelper.close();
				Intent iReservation = new Intent(ActivityCart.this, ActivityCheckout.class);
				startActivity(iReservation);
				overridePendingTransition(R.anim.open_next, R.anim.close_next);
			}
			else
			{

				//Crouton.makeText(ActivityCart.this, "Please Login to Continue", Style.INFO).show();
				Intent intent = loginBuilder.with(ActivityCart.this)
						.setAppLogo(R.drawable.ic_launcher)
						.isFacebookLoginEnabled(false).withFacebookAppId("1009002229220990")
						.isGoogleLoginEnabled(true)
						.isCustomLoginEnabled(false,SmartLoginConfig.LoginType.withEmail)
						.build();
				startActivityForResult(intent, SmartLoginConfig.LOGIN_REQUEST);
			}

			}
		});
      
    }


	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.cartmenu, menu);
		setNavigationList(menu);
		return true;
	}

	private boolean didUserLocationSaved() {
		SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
		boolean mUserLocationSaved  = sharedPreferences.getBoolean("Location", false);
		return mUserLocationSaved;
	}

	public void setNavigationList(Menu menu) {
		MenuItem mi=menu.findItem(R.id.ic_cart_menu);
		mi.setTitle("Clear Cart");
		mi.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
			@Override
			public boolean onMenuItemClick(MenuItem item) {

				showClearDialog(CLEAR_ALL_ORDER, 1111);
				return false;
			}
		});
	/*	if(!didUserLocationSaved())
		{

			mi.setTitle("Add Location");
		}
		else
		{
			DBHelper dataBaseHelper=new DBHelper(getApplicationContext());
			mi.setTitle(dataBaseHelper.getLocationName());

		}
		mi.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
			@Override
			public boolean onMenuItemClick(MenuItem menuItem) {

				Intent in=new Intent(ActivityCart.this,SetLocationActivity.class);
				startActivity(in);
				overridePendingTransition(R.anim.open_next, R.anim.close_next);

				return false;
			}
		});*/

	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		switch (item.getItemId()) {
			
		case android.R.id.home:
            // app icon in action bar clicked; go home
        	this.finish();
        	overridePendingTransition(R.anim.open_main, R.anim.close_next);
			return true;
			
		default:
			return super.onOptionsItemSelected(item);
		}
	}
    
    // method to create dialog
    void showClearDialog(int flag, int id){
    	FLAG = flag;
    	ID = id;
		AlertDialog.Builder builder = 	new AlertDialog.Builder(this);
		builder.setTitle(R.string.confirm);
		switch(FLAG){
		case 0:
			builder.setMessage(getString(R.string.clear_all_order));
			break;
		case 1:
			builder.setMessage(getString(R.string.clear_one_order));
			break;
		}
		builder.setCancelable(false);
		builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
			
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				switch(FLAG){
				case 0:
					// clear all menu in order table
					dbhelper.openDataBase();
					dbhelper.deleteAllData();
	    			listOrder.invalidateViews();
	    			clearData();
					new getDataTask().execute();
					break;
				case 1:
					// clear selected menu in order table
					dbhelper.openDataBase();
					dbhelper.deleteData(ID);
	    			listOrder.invalidateViews();
	    			clearData();
					new getDataTask().execute();
					break;
				}
				
			}
		});
		
		builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
			
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				// close dialog
				dialog.cancel();
			}
		});
		AlertDialog alert = builder.create();
		alert.show();
    }
    
    // asynctask class to handle parsing json in background
    public class getTaxCurrency extends AsyncTask<Void, Void, Void>{
    	
    	// show progressbar first
		getTaxCurrency(){
	 		if(!prgLoading.isShown()){
	 			prgLoading.setVisibility(View.VISIBLE);
				txtAlert.setVisibility(View.INVISIBLE);
	 		}
	 	}
		
		@Override
		protected Void doInBackground(Void... arg0) {
			// TODO Auto-generated method stub
			// parse json data from server in background
			parseJSONDataTax();
			return null;
		}
    	
		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			// when finish parsing, hide progressbar
			prgLoading.setVisibility(View.INVISIBLE);
			// if internet connection available request data form server
			// otherwise, show alert text
			if(IOConnect == 0){
				new getDataTask().execute();
			}else{
				txtAlert.setVisibility(View.VISIBLE);
				txtAlert.setText(R.string.alert);
			}
			
		}
    }

    // method to parse json data from server
	public void parseJSONDataTax(){
		
		try {
	        // request data from tax and currency API
	        HttpClient client = new DefaultHttpClient();
	        HttpConnectionParams.setConnectionTimeout(client.getParams(), 15000);
			HttpConnectionParams.setSoTimeout(client.getParams(), 15000);
	        HttpUriRequest request = new HttpGet(TaxCurrencyAPI);
			HttpResponse response = client.execute(request);
			InputStream atomInputStream = response.getEntity().getContent();
	
			BufferedReader in = new BufferedReader(new InputStreamReader(atomInputStream));
		        
	        String line;
	        String str = "";
	        while ((line = in.readLine()) != null){
	        	str += line;
	        }
    
	        // parse json data and store into tax and currency variables
			JSONObject json = new JSONObject(str);
			JSONArray data = json.getJSONArray("data"); // this is the "items: [ ] part
			
			JSONObject object_tax = data.getJSONObject(0); 
			JSONObject tax = object_tax.getJSONObject("tax_n_currency");
			    
			Tax = Double.parseDouble(tax.getString("Value"));
			
			JSONObject object_currency = data.getJSONObject(1); 
			JSONObject currency = object_currency.getJSONObject("tax_n_currency");
			
			Currency = currency.getString("Value");
			    
			
		} catch (MalformedURLException e) {
		    // TODO Auto-generated catch block
		    e.printStackTrace();
		} catch (IOException e) {
		    // TODO Auto-generated catch block
			IOConnect = 1;
		    e.printStackTrace();
		} catch (JSONException e) {
		    // TODO Auto-generated catch block
		    e.printStackTrace();
		}	
	}
    
	// clear arraylist variables before used
    void clearData(){
    	Menu_ID.clear();
    	Menu_name.clear();
    	Quantity.clear();
    	Sub_total_price.clear();
    }
    
    // asynctask class to handle parsing json in background
    public class getDataTask extends AsyncTask<Void, Void, Void>{
    	
    	// show progressbar first
    	getDataTask(){
    		if(!prgLoading.isShown()){
    			prgLoading.setVisibility(View.VISIBLE);
    			lytOrder.setVisibility(View.INVISIBLE);
    			txtAlert.setVisibility(View.INVISIBLE);
    		}
    	}
    	
    	@Override
		protected Void doInBackground(Void... arg0) {
			// TODO Auto-generated method stub
    		// get data from database
    		getDataFromDatabase();
			return null;
		}
    	
		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			// show data
			txtTotal.setText( Total_price+" "+Currency);
			txtTotalLabel.setText(getString(R.string.total_order)+" (Tax "+Tax+"%)");
			prgLoading.setVisibility(View.INVISIBLE);
			// if data available show data on list
			// otherwise, show alert text
			if(Menu_ID.size() > 0){
				lytOrder.setVisibility(View.VISIBLE);
				listOrder.setAdapter(mola);
			}else{
				txtAlert.setVisibility(View.VISIBLE);
			}
			
		}
    }
    
    // method to get data from server
    public void getDataFromDatabase(){
    	
    	Total_price = 0;
    	clearData();
    	data = dbhelper.getAllData();
    	
    	// store data to arraylist variables
    	for(int i=0;i<data.size();i++){
    		ArrayList<Object> row = data.get(i);
    		
    		Menu_ID.add(Integer.parseInt(row.get(0).toString()));
    		Menu_name.add(row.get(1).toString());
    		Quantity.add(Integer.parseInt(row.get(2).toString()));
    		Sub_total_price.add(Double.parseDouble(formatData.format(Double.parseDouble(row.get(3).toString()))));
    		Total_price += Sub_total_price.get(i);
    	}
    	
    	// count total order
    	Total_price -= (Total_price * (Tax/100));
    	Total_price = Double.parseDouble(formatData.format(Total_price));
    }
    
    // when back button pressed close database and back to previous page
    @Override
    public void onBackPressed() {
    	// TODO Auto-generated method stub
    	super.onBackPressed();
    	dbhelper.close();
    	finish();
    	overridePendingTransition(R.anim.open_main, R.anim.close_next);
    }
    
	 
    @Override
	public void onConfigurationChanged(final Configuration newConfig)
	{
	    // Ignore orientation change to keep activity from restarting
	    super.onConfigurationChanged(newConfig);
	}
	SharedPreferences sharedPreferences;
	private boolean mUserLoggedIn = false;
	private static  final String userLogin="userLogin";
	private static  final String s_username="username";
	private static  final String s_Email="email";
	private static  final String s_Phone="phone";

	private boolean didUserLoggedin()
	{
		SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
		mUserLoggedIn=sharedPreferences.getBoolean(userLogin,false);
		return mUserLoggedIn;
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		//Intent "data" contains the user object
		if(resultCode == SmartLoginConfig.FACEBOOK_LOGIN_REQUEST){
			SmartFacebookUser user;
			try {
				user = data.getParcelableExtra(SmartLoginConfig.USER);
				currentUser=user;
				//use this user object as per your requirement
			}catch (Exception e){
				Log.e(getClass().getSimpleName(), e.getMessage());
			}
		}else if(resultCode == SmartLoginConfig.GOOGLE_LOGIN_REQUEST){
			SmartGoogleUser user;
			SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
			try {
				user = data.getParcelableExtra(SmartLoginConfig.USER);
				currentUser=user;
				user = data.getParcelableExtra(SmartLoginConfig.USER);
				currentUser=user;
				System.out.println(user);
				mUserLoggedIn=true;
				sharedPreferences.edit().putBoolean(userLogin, mUserLoggedIn).apply();
				sharedPreferences.edit().putString(s_username,user.getDisplayName()).apply();
				AccountManager manager = (AccountManager) getSystemService(ACCOUNT_SERVICE);
				Account[] list = manager.getAccounts();

				sharedPreferences.edit().putString(s_Email,list[0].toString()).apply();
				//use this user object as per your requirement
			}catch (Exception e){
				Log.e(getClass().getSimpleName(), e.getMessage());
			}
		}else if(resultCode == SmartLoginConfig.CUSTOM_LOGIN_REQUEST){
			SmartUser user = data.getParcelableExtra(SmartLoginConfig.USER);
			currentUser=user;
			//use this user object as per your requirement
		}else if(resultCode == RESULT_CANCELED){
			//Login Failed
		}
		else if (resultCode == PLACE_PICKER_REQUEST) {
			if (resultCode == RESULT_OK) {
				Place place = PlacePicker.getPlace(data, this);
				place.getAddress();
				LatLng ll = place.getLatLng();

				final String cusLat = String.valueOf(ll.latitude);
				final String cusLon = String.valueOf(ll.longitude);
				try {
					addresses = geocoder.getFromLocation(ll.latitude, ll.longitude, 1);
				} catch (IOException ex) {
					ex.printStackTrace();
				}
//                String API="AIzaSyAMI359r57a3S546PL5SIbe3BBl8YCglso";
//                String DDUrl="https://maps.googleapis.com/maps/api/distancematrix/json?units=metric&origins="+cusLat+","+cusLon+"&destinations="+chefLat+"%2C"+chefLon+"%7C&key="+API;

				String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
				final String city = addresses.get(0).getLocality();
				String state = addresses.get(0).getAdminArea();
				String country = addresses.get(0).getCountryName();
				final String postalCode = addresses.get(0).getPostalCode();
				String knownName = addresses.get(0).getFeatureName();
				final String adrString = address + " ," + city + " ," + state + " ," + country;

                  /*  Log.i("address",address);
                    Log.i("city",city);
                    Log.i("state",state);
                    Log.i("country",country);
                    Log.i("postalCode",postalCode);
                    Log.i("knownName",knownName);*/
				View adv = getLayoutInflater().inflate(R.layout.addressdialog, null, false);
				final EditText CompleteAddress = (EditText) adv.findViewById(R.id.fullAddr);
				final EditText bnfn = (EditText) adv.findViewById(R.id.bnfn);
				final EditText adrphone = (EditText) adv.findViewById(R.id.adrphone);
				CompleteAddress.setText(adrString);
				boolean wrapInScrollView = true;
				new MaterialDialog.Builder(ActivityCart.this)
						.autoDismiss(false)
						.title("Add Address")
						.customView(adv, wrapInScrollView)
						.positiveText("Confirm Address")
						.onPositive(new MaterialDialog.SingleButtonCallback() {
							@Override
							public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {

								if(CompleteAddress.getText().toString().equals(""))
								{
									CompleteAddress.setError("Address not captured please try again");
								}
								else if(bnfn.getText().toString().equals(""))
								{
									bnfn.setError("Building / Flat No is Required");
								}
								else if(adrphone.getText().toString().equals(""))
								{
									adrphone.setError("Phone number is required");
								}
								else
								{
									Toast.makeText(getApplicationContext(), "Address addded", Toast.LENGTH_SHORT).show();
									dbhelper.addAddr(currentUser.getUsername(),bnfn.getText().toString()+" ,"+CompleteAddress.getText().toString(),city,postalCode,adrphone.getText().toString(),currentUser.getEmail(),cusLat,cusLon);
									dialog.dismiss();
								}



							}
						})
						.negativeText("Change Address")
						.onNegative(new MaterialDialog.SingleButtonCallback() {
							@Override
							public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {

								PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
								try {
									startActivityForResult(builder.build(ActivityCart.this), PLACE_PICKER_REQUEST);
								} catch (GooglePlayServicesRepairableException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								} catch (GooglePlayServicesNotAvailableException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
							}
						})
						.show();


			}
		}

	}
    
}
