package com.chefmonster.android;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;

import java.util.ArrayList;

public class Myorders extends Activity {

    AdapterMyOrders moa;
    ListView ordersList;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_myorders);

        DBHelper db=new DBHelper(getApplicationContext());

        ArrayList<Orders> Order=db.getAllOrders();
        moa=new AdapterMyOrders(this,Order);
        ordersList=(ListView)findViewById(R.id.myorderlist);
        ordersList.setAdapter(moa);
        moa.notifyDataSetChanged();
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        setNavigationList(menu);
        return true;
    }

    private boolean didUserLocationSaved() {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        boolean mUserLocationSaved  = sharedPreferences.getBoolean("Location", false);
        return mUserLocationSaved;
    }

    public void setNavigationList(Menu menu) {
        MenuItem mi=menu.findItem(R.id.ic_menu);
        if(!didUserLocationSaved())
        {

            mi.setTitle("Add Location");
        }
        else
        {
            DBHelper dataBaseHelper=new DBHelper(getApplicationContext());
            mi.setTitle(dataBaseHelper.getLocationName());

        }
        mi.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {

                Intent in=new Intent(Myorders.this,SetLocationActivity.class);
                startActivity(in);
                overridePendingTransition(R.anim.open_next, R.anim.close_next);

                return false;
            }
        });

    }
}
