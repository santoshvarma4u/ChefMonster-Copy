package com.chefmonster.android;

import android.content.Context;
import android.content.Intent;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.chefmonster.android.OrdersList.OrderStatus;

import java.util.ArrayList;

import me.drakeet.materialdialog.MaterialDialog;

/**
 * Created by kalya on 27-02-2016.
 */
public class AdapterMyOrders extends BaseAdapter {

    private Context context;
    ArrayList<Orders>  order;

    private final SparseBooleanArray mCollapsedStatus;

    private LayoutInflater mInflater;

    public AdapterMyOrders(Context ctx, ArrayList<Orders> Order) {
        this.order=Order;
        this.context = ctx;
        mInflater = LayoutInflater.from(ctx);
        mCollapsedStatus=new SparseBooleanArray();
    }
    @Override
    public int getCount() {
        return order.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        final ViewHolder holder;
        if(convertView == null){
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.myorderitem, null);
            holder = new ViewHolder();

            convertView.setTag(holder);
        }else{
            holder = (ViewHolder) convertView.getTag();
        }

        Log.e("Myorders", "Myorders");
        holder.txtOrderID=(TextView)convertView.findViewById(R.id.txtMyOrderID);
        holder.txtOrderDate=(TextView)convertView.findViewById(R.id.txtOrderDate);
        holder.txtOrderInfo=(TextView)convertView.findViewById(R.id.expand_text_view);
        holder.txtOrderID.setText(order.get(position).getTxtOrderID());
        holder.txtOrderDate.setText(order.get(position).getTxtOrderDate());
        holder.txtOrderInfo.setText(order.get(position).getTxtOrderInfo());
        holder.txtOrderInfo.setMovementMethod(new ScrollingMovementMethod());
        final String orderinfo=order.get(position).getTxtOrderInfo();
        final String orderdate=order.get(position).getTxtOrderDate();
        holder.txtOrderInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent= new Intent(context, OrderStatus.class);
                intent.putExtra("Orderinfo",orderinfo);
                intent.putExtra("OrderDate",orderdate);
                context.startActivity(intent);

               /* final MaterialDialog mMaterialDialog = new MaterialDialog(context);
                mMaterialDialog.setTitle("Order Description");
                mMaterialDialog.setMessage(orderinfo);
                mMaterialDialog.setPositiveButton("OK", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mMaterialDialog.dismiss();

                    }
                });

                mMaterialDialog.show();
*/
            }
        });




       // holder.expTv1 = (ExpandableTextView) convertView.findViewById(R.id.expand_text_view);

// IMPORTANT - call setText on the ExpandableTextView to set the text content to display
        //holder.expTv1.setText(order.get(position).getTxtOrderInfo(),mCollapsedStatus,position);

        Log.d("Repeating",order.get(position).getTxtOrderInfo().toString());
        return convertView;
    }

    static class ViewHolder {
        TextView txtOrderID,txtOrderDate,txtOrderInfo;
      //  ExpandableTextView expTv1;
    }
}
