package com.chefmonster.android;

import java.io.IOException;
import java.util.ArrayList;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.AlertDialog;
import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.database.SQLException;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.chefmonster.android.Address.SelectAddress;
import com.chefmonster.android.Login.SmartLoginBuilder;
import com.chefmonster.android.Login.SmartLoginConfig;
import com.chefmonster.android.Login.manager.UserSessionManager;
import com.chefmonster.android.Login.users.SmartFacebookUser;
import com.chefmonster.android.Login.users.SmartGoogleUser;
import com.chefmonster.android.Login.users.SmartUser;
import com.github.florent37.materialviewpager.MaterialViewPager;

import me.drakeet.materialdialog.MaterialDialog;

@SuppressLint("NewApi")
public class MainActivity extends FragmentActivity implements ActionBar.OnNavigationListener {
	private DrawerLayout mDrawerLayout;
	private ListView mDrawerList;
	private ActionBarDrawerToggle mDrawerToggle;

	SharedPreferences sharedPreferences;
	private boolean mUserLoggedIn = false;
	private boolean mUserLocationSaved = false;
	private static final String FIRST_TIME = "first_time";
	private static  final String LocationStage="Location";
	private static  final String userLogin="userLogin";
	private static  final String s_username="username";
	private static  final String s_Email="email";
	private static  final String s_Phone="phone";




	// nav drawer title
	private CharSequence mDrawerTitle;

	// used to store app title
	private CharSequence mTitle;

	// slide menu items
	private String[] navMenuTitles;
	private TypedArray navMenuIcons;

	private ArrayList<NavDrawerItem> navDrawerItems;
	private AdapterNavDrawerList adapter;

	// declare dbhelper and adapter object
	static DBHelper dbhelper;
	AdapterMainMenu mma;
	SmartUser currentUser;

	TextView navUsername,navEmail,navPhone,navViewProfile;

	private MaterialViewPager mViewPager;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.nav_drawer_main);




		final SmartLoginBuilder loginBuilder = new SmartLoginBuilder();
		mTitle = mDrawerTitle = getTitle();

		// load slide menu items
		navMenuTitles = getResources().getStringArray(R.array.nav_drawer_items);

		// nav drawer icons from resources
		navMenuIcons = getResources().obtainTypedArray(R.array.nav_drawer_icons);

		mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
		mDrawerList = (ListView) findViewById(R.id.list_slidermenu);


		sharedPreferences=PreferenceManager.getDefaultSharedPreferences(MainActivity.this);

		View headerView = getLayoutInflater().inflate(R.layout.nav_header , null , false);

		navUsername=(TextView)headerView.findViewById(R.id.txtnavusername);
		navEmail=(TextView)headerView.findViewById(R.id.txtnavuseremail);
		navPhone=(TextView)headerView.findViewById(R.id.txtnavuserphone);
		navViewProfile=(TextView)headerView.findViewById(R.id.txtnavviewprofile);



		currentUser = UserSessionManager.getCurrentUser(this);
		if(didUserLoggedin())
		{
			navUsername.setText(sharedPreferences.getString(s_username,null));
			navEmail.setText(sharedPreferences.getString(s_Email,null));

		}
		else
		{
			navUsername.setText("Guest");
			navEmail.setVisibility(View.INVISIBLE);
			navPhone.setVisibility(View.INVISIBLE);
			navViewProfile.setText("Login");
		}

		navViewProfile.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if(!didUserLoggedin())
				{
					Intent intent = loginBuilder.with(MainActivity.this)
							.setAppLogo(R.drawable.ic_launcher)
							.isFacebookLoginEnabled(true).withFacebookAppId("1009002229220990")
							.isGoogleLoginEnabled(true)
							.isCustomLoginEnabled(true,SmartLoginConfig.LoginType.withEmail)
							.build();
					startActivityForResult(intent, SmartLoginConfig.LOGIN_REQUEST);
				}
			}
		});
		mDrawerList.addHeaderView(headerView);

		mDrawerLayout.setDrawerShadow(R.drawable.navigation_drawer_shadow, GravityCompat.START);

		navDrawerItems = new ArrayList<NavDrawerItem>();

		// adding nav drawer items to array
		navDrawerItems.add(new NavDrawerItem(navMenuTitles[0], navMenuIcons.getResourceId(0, -1)));
		navDrawerItems.add(new NavDrawerItem(navMenuTitles[1], navMenuIcons.getResourceId(1, -1)));
		navDrawerItems.add(new NavDrawerItem(navMenuTitles[2], navMenuIcons.getResourceId(2, -1)));
		navDrawerItems.add(new NavDrawerItem(navMenuTitles[3], navMenuIcons.getResourceId(3, -1)));

		/*navDrawerItems.add(new NavDrawerItem(navMenuTitles[4], navMenuIcons.getResourceId(4, -1)));
		navDrawerItems.add(new NavDrawerItem(navMenuTitles[5], navMenuIcons.getResourceId(5, -1)));
		navDrawerItems.add(new NavDrawerItem(navMenuTitles[6], navMenuIcons.getResourceId(6, -1)));

		navDrawerItems.add(new NavDrawerItem(navMenuTitles[7], navMenuIcons.getResourceId(7, -1)));
		navDrawerItems.add(new NavDrawerItem(navMenuTitles[8], navMenuIcons.getResourceId(8, -1)));*/
		navDrawerItems.add(new NavDrawerItem(navMenuTitles[4], navMenuIcons.getResourceId(9, -1)));

		// Recycle the typed array
		navMenuIcons.recycle();

		mDrawerList.setOnItemClickListener(new SlideMenuClickListener());

		// setting the nav drawer list adapter
		adapter = new AdapterNavDrawerList(getApplicationContext(), navDrawerItems);
		mDrawerList.setAdapter(adapter);

		// enabling action bar app icon and behaving it as toggle button
		getActionBar().setDisplayHomeAsUpEnabled(true);
		getActionBar().setHomeButtonEnabled(true);
		getActionBar().setDisplayShowTitleEnabled(true);



		// get screen device width and height
		DisplayMetrics dm = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(dm);

		// checking internet connection
		if (!Constant.isNetworkAvailable(MainActivity.this)) {
			Toast.makeText(MainActivity.this, getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
		}

		if(!didUserLocationSaved())
		{
			final MaterialDialog mMaterialDialog = new MaterialDialog(MainActivity.this);
			mMaterialDialog.setTitle("Location Alert");
			mMaterialDialog.setMessage("Please select your preferred location, to get the tasty foods near by you.");
			mMaterialDialog.setPositiveButton("Select Location", new View.OnClickListener() {
				@Override
				public void onClick(View v) {

					Intent in=new Intent(MainActivity.this,SelectAddress.class);
					startActivity(in);
					overridePendingTransition(R.anim.open_next, R.anim.close_next);
				}
			});
			mMaterialDialog.show();
		}
		mma = new AdapterMainMenu(this);
		dbhelper = new DBHelper(this);

		// create database
		try {
			dbhelper.createDataBase();
		} catch (IOException ioe) {
			throw new Error("Unable to create database");
		}

		// then, the database will be open to use
		try {
			dbhelper.openDataBase();
		} catch (SQLException sqle) {
			throw sqle;
		}

		// if user has already ordered food previously then show confirm dialog
		if (dbhelper.isPreviousDataExist()) {
			//showAlertDialog();
		}
		mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.drawable.ic_menu_black_36dp, // nav
				// toggle
				// icon
				R.string.app_name, // nav drawer open - description for
									// accessibility
				R.string.app_name // nav drawer close - description for
									// accessibility
		) {
			public void onDrawerClosed(View view) {
				getActionBar().setTitle(mTitle);
				// calling onPrepareOptionsMenu() to show action bar icons
				invalidateOptionsMenu();
			}

			public void onDrawerOpened(View drawerView) {
			getActionBar().setTitle(mDrawerTitle);
				// calling onPrepareOptionsMenu() to hide action bar icons
				invalidateOptionsMenu();
			}
		};
		mDrawerLayout.setDrawerListener(mDrawerToggle);

		if (savedInstanceState == null) {
			// on first time display view for first nav item
			displayView(0);
		}
	}

	// show confirm dialog to ask user to delete previous order or not
	void showAlertDialog() {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle(R.string.confirm);
		builder.setMessage(getString(R.string.db_exist_alert));
		builder.setCancelable(false);
		builder.setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {

			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				// delete order data when yes button clicked
				dbhelper.deleteAllData();
				dbhelper.close();

			}
		});

		builder.setNegativeButton(getString(R.string.no), new DialogInterface.OnClickListener() {

			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				// close dialog when no button clicked
				dbhelper.close();
				dialog.cancel();
			}
		});
		AlertDialog alert = builder.create();
		alert.show();

	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		dbhelper.deleteAllData();
		dbhelper.close();
		finish();
		overridePendingTransition(R.anim.open_main, R.anim.close_next);
	}

	@Override
	public boolean onNavigationItemSelected(int i, long l) {
		return false;
	}

	/**
	 * Slide menu item click listener
	 */
	private class SlideMenuClickListener implements ListView.OnItemClickListener {
		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
			// display view for selected nav drawer item
			position=position-1;
			displayView(position);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		setNavigationList(menu);
		return true;
	}
	public void setNavigationList(Menu menu) {
		MenuItem mi=menu.findItem(R.id.ic_menu);
		if(!didUserLocationSaved())
		{

			mi.setTitle("Add Location");
		}
		else
		{
			DBHelper dataBaseHelper=new DBHelper(getApplicationContext());
			mi.setTitle(dataBaseHelper.getLocationName());
		}
		mi.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
			@Override
			public boolean onMenuItemClick(MenuItem menuItem) {

				Intent in=new Intent(MainActivity.this,SelectAddress.class);
				startActivity(in);
				overridePendingTransition(R.anim.open_next, R.anim.close_next);

				return false;
			}
		});

	}
	/*
	 * * Called when invalidateOptionsMenu() is triggered
	 */
	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		// if nav drawer is opened, hide the action items
		boolean drawerOpen = mDrawerLayout.isDrawerOpen(mDrawerList);
		menu.findItem(R.id.ic_menu).setVisible(!drawerOpen);
		return super.onPrepareOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (mDrawerToggle.onOptionsItemSelected(item)) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	private boolean didUserLocationSaved() {
		SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
		mUserLocationSaved = sharedPreferences.getBoolean(LocationStage, false);
		return mUserLocationSaved;
	}
	private boolean didUserLoggedin()
	{
		mUserLoggedIn=sharedPreferences.getBoolean(userLogin,false);
		return mUserLoggedIn;
	}
	/**
	 * Diplaying fragment view for selected nav drawer list item
	 */
	private void displayView(int position) {
		// update the main content by replacing fragments
		Fragment fragment = null;
		switch (position) {
		case 0:
			fragment = new ActivityMenuList();
			break;
		case 1:
			fragment = new ActivityDeals();
			overridePendingTransition(R.anim.open_next, R.anim.close_next);
			break;
		case 2:
			startActivity(new Intent(getApplicationContext(), ActivityCart.class));
			overridePendingTransition(R.anim.open_next, R.anim.close_next);
			break;
		case 3:
			startActivity(new Intent(getApplicationContext(), Myorders.class));
			overridePendingTransition(R.anim.open_next, R.anim.close_next);
			break;
		case 4:
			dbhelper.deleteAllData();
			dbhelper.close();
			MainActivity.this.finish();
			overridePendingTransition(R.anim.open_next, R.anim.close_next);
			break;
		case 5:
			startActivity(new Intent(getApplicationContext(), ActivityInformation.class));
			overridePendingTransition(R.anim.open_next, R.anim.close_next);
			break;
		case 6:
			startActivity(new Intent(getApplicationContext(), ActivityAbout.class));
			overridePendingTransition(R.anim.open_next, R.anim.close_next);

			break;
		case 7:
			Intent sendInt = new Intent(Intent.ACTION_SEND);
			sendInt.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.app_name));
			sendInt.putExtra(Intent.EXTRA_TEXT, "E-Commerce Android App\n\"" + getString(R.string.app_name)
					+ "\" \nhttps://play.google.com/store/apps/details?id=" + getPackageName());
			sendInt.setType("text/plain");
			startActivity(Intent.createChooser(sendInt, "Share"));
			break;
		case 8:
			startActivity(new Intent(getApplicationContext(), ActivityContactUs.class));
			overridePendingTransition(R.anim.open_next, R.anim.close_next);

			break;
		case 9:
			dbhelper.deleteAllData();
			dbhelper.close();
			MainActivity.this.finish();
			overridePendingTransition(R.anim.open_next, R.anim.close_next);

			break;

		default:
			break;
		}

		if (fragment != null) {
			android.app.FragmentManager fragmentManager = getFragmentManager();
			fragmentManager.beginTransaction().replace(R.id.frame_container, fragment).commit();

			// update selected item and title, then close the drawer
			mDrawerList.setItemChecked(position, true);
			mDrawerList.setSelection(position);
			//setTitle(navMenuTitles[position]);
			mDrawerLayout.closeDrawer(mDrawerList);
		} else {
			// error in creating fragment
			Log.e("MainActivity", "Error in creating fragment");
		}
	}

	/*@Override
	public void setTitle(CharSequence title) {
		mTitle = title;
		getActionBar().setTitle(mTitle);
	}*/

	/**
	 * When using the ActionBarDrawerToggle, you must call it during
	 * onPostCreate() and onConfigurationChanged()...
	 */

	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);
		// Sync the toggle state after onRestoreInstanceState has occurred.
		mDrawerToggle.syncState();
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		// Pass any configuration change to the drawer toggls
		mDrawerToggle.onConfigurationChanged(newConfig);
	}


	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		//Intent "data" contains the user object
		if(resultCode == SmartLoginConfig.FACEBOOK_LOGIN_REQUEST){
			SmartFacebookUser user;
			try {
				user = data.getParcelableExtra(SmartLoginConfig.USER);
				currentUser=user;
				mUserLoggedIn=true;
				sharedPreferences.edit().putBoolean(userLogin, mUserLoggedIn).apply();
				sharedPreferences.edit().putString(s_username,user.getUsername().toString()).apply();
				sharedPreferences.edit().putString(s_Email,user.getEmail().toString()).apply();
				navUsername.setText(sharedPreferences.getString(s_username,null));
				navEmail.setText(sharedPreferences.getString(s_Email,null));
				//use this user object as per your requirement
			}catch (Exception e){
				Log.e(getClass().getSimpleName(), e.getMessage());
			}
		}else if(resultCode == SmartLoginConfig.GOOGLE_LOGIN_REQUEST){
			SmartGoogleUser user;
			try {
				user = data.getParcelableExtra(SmartLoginConfig.USER);
				currentUser=user;
				System.out.println(user);
				mUserLoggedIn=true;
				sharedPreferences.edit().putBoolean(userLogin, mUserLoggedIn).apply();
				sharedPreferences.edit().putString(s_username,user.getDisplayName()).apply();
				AccountManager manager = (AccountManager) getSystemService(ACCOUNT_SERVICE);
				Account[] list = manager.getAccounts();

				sharedPreferences.edit().putString(s_Email,list[0].toString()).apply();
				navUsername.setText(sharedPreferences.getString(s_username,null));
				navEmail.setText(sharedPreferences.getString(s_Email,null));
				//use this user object as per your requirement
			}catch (Exception e){
				Log.e(getClass().getSimpleName(), e.getMessage());
			}
		}else if(resultCode == SmartLoginConfig.CUSTOM_LOGIN_REQUEST){
			SmartUser user = data.getParcelableExtra(SmartLoginConfig.USER);
			currentUser=user;
			mUserLoggedIn=true;
			sharedPreferences.edit().putBoolean(userLogin, mUserLoggedIn).apply();
			sharedPreferences.edit().putString(s_username,user.getUsername().toString()).apply();
			sharedPreferences.edit().putString(s_Email,user.getEmail().toString()).apply();
			navUsername.setText(sharedPreferences.getString(s_username,null));
			navEmail.setText(sharedPreferences.getString(s_Email,null));
			//use this user object as per your requirement
		}else if(resultCode == RESULT_CANCELED){
			//Login Failed
		}
	}
}
