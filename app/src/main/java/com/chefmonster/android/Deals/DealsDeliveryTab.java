package com.chefmonster.android.Deals;

import android.content.Context;
import android.view.View;
import android.widget.TabHost;

/**
 * Created by tskva on 7/4/2016.
 */
public class DealsDeliveryTab implements TabHost.TabContentFactory {
    private Context mContext;

    public DealsDeliveryTab(Context context){
        mContext = context;
    }

    @Override
    public View createTabContent(String tag) {
        View v = new View(mContext);
        return v;
    }
}
