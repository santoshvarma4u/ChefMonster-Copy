package com.chefmonster.android.Deals;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.chefmonster.android.ActivityDeals;
import com.chefmonster.android.Constant;
import com.chefmonster.android.DBHelper;
import com.chefmonster.android.ImageLoader;
import com.chefmonster.android.R;

import java.text.DecimalFormat;
import java.util.ArrayList;

/**
 * Created by tskva on 7/4/2016.
 */
public class DealsDeliveryAdapter extends BaseAdapter {
    private Activity activity;
    public ImageLoader imageLoader;
    private TextView mTxtAmountAdapter;

    public DealsDeliveryAdapter(Activity act) {
        this.activity = act;
        imageLoader = new ImageLoader(act);
    }

    public int getCount() {
        // TODO Auto-generated method stub
        return DealsDeliveryFragment.Menu_ID.size();
    }

    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    public View getView(final int position, View convertView, final ViewGroup parent) {
        // TODO Auto-generated method stub
        final ViewHolder holder;

        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.deals_delivery_list_item, null);
            holder = new ViewHolder();

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.txtText = (TextView) convertView.findViewById(R.id.txtTextdealsdelivery);
        holder.txtSubText = (TextView) convertView.findViewById(R.id.txtSubTextdealsdelivery);
        holder.imgThumb = (ImageView) convertView.findViewById(R.id.imgThumbdealsdelivery);
        holder.txtweigthingms = (TextView) convertView.findViewById(R.id.txtweightgmsdealsdelivery);
        holder.txtStockQty = (TextView) convertView.findViewById(R.id.txtStockQtydealsdelivery);
        holder.txtfoodtype = (TextView) convertView.findViewById(R.id.txtfoodtypedealsdelivery);
        holder.txtDeliverytime = (TextView) convertView.findViewById(R.id.txtDtimedealsdelivery);

        holder.txtText.setText(DealsDeliveryFragment.Menu_name.get(position));
        holder.txtSubText.setText(DealsDeliveryFragment.Menu_price.get(position) + " " + DealsDeliveryFragment.Currency);
        holder.txtDeliverytime.setText(DealsDeliveryFragment.Menu_delivery_time.get(position));
        holder.txtfoodtype.setText(DealsDeliveryFragment.Menu_food_type.get(position));
        holder.txtStockQty.setText(DealsDeliveryFragment.Menu_stock_qty.get(position));
        holder.txtweigthingms.setText(DealsDeliveryFragment.Menu_weigth_grms.get(position));

        holder.txtQty = (TextView) convertView.findViewById(R.id.txtQtydealsdelivery);
        holder.btninc = (Button) convertView.findViewById(R.id.btnincdealsdelivery);
        holder.btndrc = (Button) convertView.findViewById(R.id.btndrcdealsdelivery);
        imageLoader.DisplayImage(Constant.AdminPageURL + DealsDeliveryFragment.Menu_image.get(position), holder.imgThumb);
        holder.btninc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int qty = Integer.parseInt(holder.txtQty.getText().toString());
                if (qty < 0) {

                } else {

                    qty++;
                    double price = DealsDeliveryFragment.Menu_price.get(position);
                    double changedPrice = price * qty;
                    holder.txtSubText.setText(changedPrice + " " + DealsDeliveryFragment.Currency);
                    holder.txtQty.setText(String.valueOf(qty));

                    DBHelper db = new DBHelper(parent.getContext());
                    db.openDataBase();
                    long menuid = DealsDeliveryFragment.Menu_ID.get(position);
                    double menuprice = DealsDeliveryFragment.Menu_price.get(position);
                    String menuname = DealsDeliveryFragment.Menu_name.get(position);
                    if (db.isDataExist(menuid)) {
                        db.updateData(menuid, qty, (menuprice * qty));
                    } else {
                        db.addData(menuid, menuname, qty, (menuprice * qty));
                    }

                    double currentCartPrice = Double.parseDouble(mTxtAmountAdapter.getText().toString());

                    double ChangedPrice = currentCartPrice + menuprice;

                    //mTxtAmountAdapter.setText(String.valueOf(ChangedPrice));
                    db.close();
                }

                mTxtAmountAdapter.setText(String.valueOf(getDataFromDatabase(parent.getContext())));
            }
        });
        holder.btndrc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int qty = Integer.parseInt(holder.txtQty.getText().toString());
                qty--;

                if (qty <= 0) {
                    double price = DealsDeliveryFragment.Menu_price.get(position);
                    holder.txtSubText.setText(price + " " + DealsDeliveryFragment.Currency);
                    holder.txtQty.setText("0");

                    long menuid = DealsDeliveryFragment.Menu_ID.get(position);
                    DBHelper db = new DBHelper(parent.getContext());
                    db.openDataBase();
                    db.deleteData(menuid);
                    db.close();

                    double currentCartPrice = Double.parseDouble(mTxtAmountAdapter.getText().toString());

                    if (currentCartPrice > 0) {
                        double ChangedPrice = currentCartPrice - price;
                        //mTxtAmountAdapter.setText(String.valueOf(ChangedPrice));
                    }

                } else {
                    if (qty == 1) {
                        double price = DealsDeliveryFragment.Menu_price.get(position);
                        holder.txtQty.setText("0");
                        holder.txtSubText.setText(price + " " + DealsDeliveryFragment.Currency);
                        DBHelper db = new DBHelper(parent.getContext());
                        db.openDataBase();
                        long menuid = DealsDeliveryFragment.Menu_ID.get(position);
                        double menuprice = DealsDeliveryFragment.Menu_price.get(position);
                        String menuname = DealsDeliveryFragment.Menu_name.get(position);
                        if (db.isDataExist(menuid)) {
                            db.updateData(menuid, qty, (menuprice * qty));
                        } else {
                            db.addData(menuid, menuname, qty, (menuprice * qty));
                        }
                        double currentCartPrice = Double.parseDouble(mTxtAmountAdapter.getText().toString());

                        double ChangedPrice = currentCartPrice - price;

                        //mTxtAmountAdapter.setText(String.valueOf(ChangedPrice));
                        db.close();
                    } else {

                        double price = DealsDeliveryFragment.Menu_price.get(position);
                        double changedPrice = price * qty;
                        holder.txtSubText.setText(changedPrice + " " + DealsDeliveryFragment.Currency);
                        holder.txtQty.setText(String.valueOf(qty));
                        DBHelper db = new DBHelper(parent.getContext());
                        db.openDataBase();
                        long menuid = DealsDeliveryFragment.Menu_ID.get(position);
                        double menuprice = DealsDeliveryFragment.Menu_price.get(position);
                        String menuname = DealsDeliveryFragment.Menu_name.get(position);
                        if (db.isDataExist(menuid)) {
                            db.updateData(menuid, qty, (menuprice * qty));
                        } else {
                            db.addData(menuid, menuname, qty, (menuprice * qty));
                        }
                        double currentCartPrice = Double.parseDouble(mTxtAmountAdapter.getText().toString());

                        double ChangedPrice = currentCartPrice - price;

                        //	mTxtAmountAdapter.setText(String.valueOf(ChangedPrice));
                        db.close();
                    }
                }
                mTxtAmountAdapter.setText(String.valueOf(getDataFromDatabase(parent.getContext())));
            }
        });
        return convertView;
    }

    public double getDataFromDatabase(Context ctx) {

        DecimalFormat formatData = new DecimalFormat("#.##");
        ArrayList<ArrayList<Object>> data;
        ArrayList<Double> Sub_total_price = new ArrayList<Double>();
        double Total_price = 0;
        DBHelper dbhelper = new DBHelper(ctx);
        dbhelper.openDataBase();
        data = dbhelper.getAllData();

        // store data to arraylist variables
        for (int i = 0; i < data.size(); i++) {
            ArrayList<Object> row = data.get(i);
            Sub_total_price.add(Double.parseDouble(formatData.format(Double.parseDouble(row.get(3).toString()))));
            Total_price += Sub_total_price.get(i);
        }

        // count total order

        //Total_price -= (Total_price * (Tax/100));
        Total_price = Double.parseDouble(formatData.format(Total_price));
        dbhelper.close();
        return Total_price;
    }

    static class ViewHolder {
        TextView txtText, txtSubText, txtQty, txtStockQty, txtDeliverytime, txtweigthingms, txtfoodtype;
        ImageView imgThumb;
        Button btninc, btndrc;
        RatingBar rb;
    }
}

