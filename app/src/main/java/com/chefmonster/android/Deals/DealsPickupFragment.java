package com.chefmonster.android.Deals;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.chefmonster.android.R;

/**
 * Created by tskva on 7/4/2016.
 */
public class DealsPickupFragment extends Fragment {
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View v = inflater.inflate(R.layout.dealspickupfragment, container, false);
        return v;
    }
}
