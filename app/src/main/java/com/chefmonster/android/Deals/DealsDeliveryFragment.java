package com.chefmonster.android.Deals;

import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.chefmonster.android.AdapterDeals;
import com.chefmonster.android.Constant;
import com.chefmonster.android.DBHelper;
import com.chefmonster.android.R;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpConnectionParams;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.text.DecimalFormat;
import java.util.ArrayList;

/**
 * Created by tskva on 7/4/2016.
 */
public class DealsDeliveryFragment extends Fragment {

    ListView listMenu;
    DealsDeliveryAdapter mla;

    // create arraylist variables to store data from server
    static ArrayList<Long> Menu_ID = new ArrayList<Long>();
    static ArrayList<String> Menu_name = new ArrayList<String>();
    static ArrayList<Double> Menu_price = new ArrayList<Double>();
    static ArrayList<String> Menu_image = new ArrayList<String>();
    static ArrayList<String> Menu_stock_qty = new ArrayList<String>();
    static ArrayList<String> Menu_food_type = new ArrayList<String>();
    static ArrayList<String> Menu_delivery = new ArrayList<String>();
    static ArrayList<String> Menu_weigth_grms = new ArrayList<String>();
    static ArrayList<String> Menu_serves = new ArrayList<String>();
    static ArrayList<String> Menu_delivery_time = new ArrayList<String>();

    static String Currency;

    String MenuAPI;
    String TaxCurrencyAPI;
    int IOConnect = 0;
    long Category_ID;
    String Category_name;
    String Keyword;
    static double Tax;
    DecimalFormat formatData = new DecimalFormat("#.##");
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View v = inflater.inflate(R.layout.dealsdeliveryfragment, container, false);
        MenuAPI = Constant.MenuAPI+"?accesskey="+Constant.AccessKey+"&category_id=";
        // tax and currency API url
        TaxCurrencyAPI = Constant.TaxCurrencyAPI+"?accesskey="+Constant.AccessKey;

        listMenu = (ListView) v.findViewById(R.id.listMenuDeliverydeals);
        // get category id and category name that sent from previous page
        Intent iGet = getActivity().getIntent();
        Category_ID = iGet.getLongExtra("category_id",0);
        //Category_name = iGet.getStringExtra("category_name");
        MenuAPI += "";

        mla = new DealsDeliveryAdapter(getActivity());

        // call asynctask class to request tax and currency data from server
        new getTaxCurrency().execute();

        return v;
    }
    public double getDataFromDatabase(Context ctx){

        DecimalFormat formatData = new DecimalFormat("#.##");
        ArrayList<ArrayList<Object>> data;
        ArrayList<Double> Sub_total_price = new ArrayList<Double>();
        double Total_price = 0;
        DBHelper dbhelper=new DBHelper(ctx);
        dbhelper.openDataBase();
        data = dbhelper.getAllData();

        // store data to arraylist variables
        for(int i=0;i<data.size();i++){
            ArrayList<Object> row = data.get(i);
            Sub_total_price.add(Double.parseDouble(formatData.format(Double.parseDouble(row.get(3).toString()))));
            Total_price +=Sub_total_price.get(i);
        }

        // count total order

        //Total_price -= (Total_price * (Tax/100));
        Total_price = Double.parseDouble(formatData.format(Total_price));
        dbhelper.close();
        return Total_price;
    }
    public class getTaxCurrency extends AsyncTask<Void, Void, Void> {

        // show progressbar first
        getTaxCurrency(){

        }

        @Override
        protected Void doInBackground(Void... arg0) {
            // TODO Auto-generated method stub
            // parse json data from server in background
            parseJSONDataTax();
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            // TODO Auto-generated method stub
            // when finish parsing, hide progressbar
           // prgLoading.setVisibility(8);
            // if internet connection and data available request menu data from server
            // otherwise, show alert text
            if((Currency != null) && IOConnect == 0){
                new getDataTask().execute();
            }else{
               // txtAlert.setVisibility(0);
            }
        }
    }
    // method to parse json data from server
    public void parseJSONDataTax(){
        try {
            // request data from tax and currency API
            HttpClient client = new DefaultHttpClient();
            HttpConnectionParams.setConnectionTimeout(client.getParams(), 15000);
            HttpConnectionParams.setSoTimeout(client.getParams(), 15000);
            HttpUriRequest request = new HttpGet(TaxCurrencyAPI);
            HttpResponse response = client.execute(request);
            InputStream atomInputStream = response.getEntity().getContent();

            BufferedReader in = new BufferedReader(new InputStreamReader(atomInputStream));

            String line;
            String str = "";
            while ((line = in.readLine()) != null){
                str += line;
            }


            // parse json data and store into tax and currency variables
            JSONObject json = new JSONObject(str);
            JSONArray data = json.getJSONArray("data"); // this is the "items: [ ] part

            JSONObject object_tax = data.getJSONObject(0);
            JSONObject tax = object_tax.getJSONObject("tax_n_currency");

            Tax = Double.parseDouble(tax.getString("Value"));

            JSONObject object_currency = data.getJSONObject(1);
            JSONObject currency = object_currency.getJSONObject("tax_n_currency");

            Currency = currency.getString("Value");
        } catch (MalformedURLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            IOConnect = 1;
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
    // asynctask class to handle parsing json in background
    public class getDataTask extends AsyncTask<Void, Void, Void>{

        // show progressbar first
        getDataTask(){

        }

        @Override
        protected Void doInBackground(Void... arg0) {
            // TODO Auto-generated method stub
            // parse json data from server in background
            parseJSONData();
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            // TODO Auto-generated method stub
            // when finish parsing, hide progressbar
            //prgLoading.setVisibility(8);

            // if data available show data on list
            // otherwise, show alert text
            if(Menu_ID.size() > 0){
                listMenu.setVisibility(0);
                listMenu.setAdapter(mla);
            }else{
                //txtAlert.setVisibility(0);
            }

        }
    }
    public void parseJSONData(){

        clearData();

        try {
            // request data from menu API
            HttpClient client = new DefaultHttpClient();
            HttpConnectionParams.setConnectionTimeout(client.getParams(), 15000);
            HttpConnectionParams.setSoTimeout(client.getParams(), 15000);
            HttpUriRequest request = new HttpGet(MenuAPI);
            HttpResponse response = client.execute(request);
            InputStream atomInputStream = response.getEntity().getContent();

            BufferedReader in = new BufferedReader(new InputStreamReader(atomInputStream));

            String line;
            String str = "";
            while ((line = in.readLine()) != null){
                str += line;
            }

            // parse json data and store into arraylist variables
            JSONObject json = new JSONObject(str);
            JSONArray data = json.getJSONArray("data"); // this is the "items: [ ] part

            for (int i = 0; i < data.length(); i++) {
                JSONObject object = data.getJSONObject(i);

                JSONObject menu = object.getJSONObject("Menu");

                Log.e("Menu",menu.toString());

                Menu_ID.add(Long.parseLong(menu.getString("Menu_ID")));
                Menu_name.add(menu.getString("Menu_name"));
                Menu_price.add(Double.valueOf(formatData.format(menu.getDouble("Price"))));
                Menu_image.add(menu.getString("Menu_image"));
                Menu_stock_qty.add(menu.getString("Quantity"));
                Menu_delivery.add(menu.getString("delivery_type"));
                Menu_delivery_time.add(menu.getString("delivery_time"));
                Menu_food_type.add(menu.getString("type_of_food"));
                Menu_serves.add(menu.getString("no_of_serves"));
                Menu_weigth_grms.add(menu.getString("weight_in_grams"));
            }


        } catch (MalformedURLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
    void clearData(){
        Menu_ID.clear();
        Menu_name.clear();
        Menu_price.clear();
        Menu_image.clear();
        Menu_stock_qty.clear();
        Menu_delivery.clear();
        Menu_delivery_time.clear();
        Menu_food_type.clear();
        Menu_serves.clear();
        Menu_weigth_grms.clear();
    }
}
