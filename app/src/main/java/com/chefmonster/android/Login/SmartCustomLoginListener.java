package com.chefmonster.android.Login;

import com.chefmonster.android.Login.users.SmartUser;


/**
 * Created by Kalyan on 9/11/2015.
 */
public interface SmartCustomLoginListener {
    boolean customSignin(SmartUser user);
    boolean customSignup(SmartUser newUser);
}
