package com.chefmonster.android.model;

/**
 * Created by tskva on 7/19/2016.
 */
public class Address {

    String Address = null;
    String Location = null;
    boolean selected = false;

    public Address(String address,String location,Boolean selected)
    {
        super();
        this.Address=address;
        this.Location=location;
        this.selected=selected;
    }
    public String getAddress() {
        return Address;
    }
    public void setAddress(String addr) {
        this.Address = addr;
    }
    public String getLocation() {
        return Location;
    }
    public void setLocation(String location) {
        this.Location = location;
    }

    public boolean isSelected() {
        return selected;
    }
    public void setSelected(boolean selected) {
        this.selected = selected;
    }
}
