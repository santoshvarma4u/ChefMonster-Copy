package com.chefmonster.android.OrdersList;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.widget.TextView;

import com.chefmonster.android.R;

public class OrderStatus extends Activity {

    TextView txtOrderInfo,txtOrderTime,txtPaymentMethod,txtDeliveryContact;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_status);
        initUI();
    }
    public void initUI()
    {
        txtOrderInfo=(TextView)findViewById(R.id.txtOrderInfo);
        txtOrderTime=(TextView)findViewById(R.id.txtOrderedOn);
        txtPaymentMethod=(TextView)findViewById(R.id.txtPaymentMode);
        txtDeliveryContact=(TextView)findViewById(R.id.txtDeliveryContact);
        //load order info

        Intent i=getIntent();
        String orderInfo=i.getStringExtra("Orderinfo").toString();
        String orderDate=i.getStringExtra("OrderDate").toString();

        txtOrderInfo.setText(orderInfo);
        txtOrderTime.setText("ChefMonster @ "+orderDate);

        getActionBar().setDisplayHomeAsUpEnabled(true);
        getActionBar().setHomeButtonEnabled(true);
        getActionBar().setDisplayShowTitleEnabled(true);
        getActionBar().setTitle("Order Tracking");

    }
}
