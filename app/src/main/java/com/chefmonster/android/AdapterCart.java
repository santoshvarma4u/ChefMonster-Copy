package com.chefmonster.android;

import android.app.Activity;
import android.app.admin.SystemUpdatePolicy;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.util.ArrayList;

// adapter class for custom order list
class AdapterCart extends BaseAdapter {
		private LayoutInflater inflater;
		public TextView totalValueCart;
		public AdapterCart(Context context,TextView total) {
			inflater = LayoutInflater.from(context);
			this.totalValueCart=total;
		}
		
		public int getCount() {
			// TODO Auto-generated method stub
			return ActivityCart.Menu_ID.size();
		}

		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		public View getView(int position, View convertView, final ViewGroup parent) {
			// TODO Auto-generated method stub
			final ViewHolder holder;
			
			if(convertView == null){
				convertView = inflater.inflate(R.layout.order_list_item, null);
				holder = new ViewHolder();
				holder.txtMenuName = (TextView) convertView.findViewById(R.id.txtMenuName);
				holder.txtQuantity = (TextView) convertView.findViewById(R.id.txtQuantity);
				holder.txtPrice = (TextView) convertView.findViewById(R.id.txtPrice);
				holder.txtMenuIDcart=(TextView) convertView.findViewById(R.id.txtMenuCartID);
				holder.btninc=(Button) convertView.findViewById(R.id.btninccart);
				holder.btndec=(Button) convertView.findViewById(R.id.btndeccart);

				final DBHelper db=new DBHelper(parent.getContext());
				db.openDataBase();
				holder.btninc.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						System.out.println("hello in inc");
						int qty = Integer.parseInt(holder.txtQuantity.getText().toString());
						double basePrice=Double.parseDouble(holder.txtPrice.getText().toString())/qty;
						if(qty>=1){
							db.openDataBase();
							qty++;
							double changedPrice=basePrice*qty;
							holder.txtQuantity.setText(String.valueOf(qty));
							holder.txtPrice.setText(String.valueOf(changedPrice));
							db.updateData(Long.parseLong(holder.txtMenuIDcart.getText().toString()), qty, changedPrice);
						}
						else{


						}
						db.openDataBase();
						totalValueCart.setText(String.valueOf(getDataFromDatabase(parent.getContext()))+" INR");
					}
				});
				holder.btndec.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						int qty = Integer.parseInt(holder.txtQuantity.getText().toString());
						double basePrice=Double.parseDouble(holder.txtPrice.getText().toString())/qty;
						if(qty<=1){
						}
						else{
							db.openDataBase();
							qty--;
							double changedPrice=basePrice*qty;
							holder.txtQuantity.setText(String.valueOf(qty));
							holder.txtPrice.setText(String.valueOf(changedPrice));
							db.updateData(Long.parseLong(holder.txtMenuIDcart.getText().toString()), qty, changedPrice);
						}
						db.openDataBase();
						totalValueCart.setText(String.valueOf(getDataFromDatabase(parent.getContext()))+" INR");
					}
				});
				convertView.setTag(holder);
			}else{
				holder = (ViewHolder) convertView.getTag();
			}

    		holder.txtMenuIDcart.setText(String.valueOf(ActivityCart.Menu_ID.get(position)));
			holder.txtMenuName.setText(ActivityCart.Menu_name.get(position));
			holder.txtQuantity.setText(String.valueOf(ActivityCart.Quantity.get(position)));
			holder.txtPrice.setText(String.valueOf(ActivityCart.Sub_total_price.get(position)));
			
			return convertView;
		}

	public double getDataFromDatabase(Context ctx){

		DecimalFormat formatData = new DecimalFormat("#.##");
		ArrayList<ArrayList<Object>> data;
		ArrayList<Double> Sub_total_price = new ArrayList<Double>();
		double Total_price = 0;
		DBHelper dbhelper=new DBHelper(ctx);
		dbhelper.openDataBase();
		data = dbhelper.getAllData();

		// store data to arraylist variables
		for(int i=0;i<data.size();i++){
			ArrayList<Object> row = data.get(i);
			Sub_total_price.add(Double.parseDouble(formatData.format(Double.parseDouble(row.get(3).toString()))));
			Total_price +=Sub_total_price.get(i);
		}

		// count total order

		//Total_price -= (Total_price * (Tax/100));
		Total_price = Double.parseDouble(formatData.format(Total_price));
		dbhelper.close();
		return Total_price;
	}

		static class ViewHolder {
			TextView txtMenuName, txtQuantity, txtPrice,txtMenuIDcart;
			Button btninc,btndec;
		}
		
		
	}