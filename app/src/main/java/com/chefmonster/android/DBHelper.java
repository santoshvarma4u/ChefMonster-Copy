package com.chefmonster.android;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.chefmonster.android.Address.AddressPojo;

public class DBHelper extends SQLiteOpenHelper{
	
	String DB_PATH;
	public final static String DB_NAME = "db_order";
	public final static int DB_VERSION = 2;
    public static SQLiteDatabase db;

	public final Context context;

	public final String TABLE_NAME = "tbl_order";
	public final String TABLE_ADDR="tbl_addr";
	public final String TABLE_ORDR="tbl_myorder";

	public static final String LOCATION_TABLE = "location";

	public final String ID = "id";
	public final String MENU_NAME = "Menu_name";
	public final String QUANTITY = "Quantity";
	public final String TOTAL_PRICE = "Total_price";

	public final String NAME="name";
	public final String ADDR="address";
	public final String CITY="city";
	public final String SHOPCODE="code";
	public final String PHONE="phone";
	public final String EMAIL="email";
	public final String LATITUDE="latitude";
	public final String LONGITUDE="longitude";

	public final String ORDERID="orderid";
	public final String ORDERDATE="order_date";
	public final String ORDERINFO="info_order";
	public final String ORDERPRICE="order_price";
	public final String ORDERTYPE="order_type";
	public final String ORDERSTATUS="order_status";



	public static final String ID_COLUMN = "id";
	public static final String NAME_LOCATION_COLUMN = "name";
	public static final String LATITUDE_COLOMN = "decription";
	public static final String LONGITUDE_COLOMN = "image";


	public static final String CREATE_USER_LOCATION_TABLE = "CREATE TABLE "
			+ LOCATION_TABLE + "(" + ID_COLUMN + " INTEGER PRIMARY KEY, "
			+ NAME_LOCATION_COLUMN + " TEXT, " + LATITUDE_COLOMN + " TEXT, "
			+ LONGITUDE_COLOMN + " TEXT )";

	
    public DBHelper(Context context) {
 
    	super(context, DB_NAME, null, DB_VERSION);
        this.context = context;
        
        DB_PATH = Constant.DBPath;
    }	
 
    public void createDataBase() throws IOException{
 
    	boolean dbExist = checkDataBase();
    	SQLiteDatabase db_Read = null;


    	if(dbExist){
    		//do nothing - database already exist

    	}else{
    		db_Read = this.getReadableDatabase();
    		db_Read.close();

			db=this.getWritableDatabase();

        	try {
    			copyDataBase();
				String CREATE_TABLE_USERS = "CREATE TABLE "
						+ TABLE_ADDR  + "(" + ID + " INTEGER PRIMARY KEY, "
						+ NAME + " TEXT, " + ADDR + " TEXT," + CITY + " TEXT ," + SHOPCODE + " TEXT , "
						+ PHONE + " TEXT, " + EMAIL + " TEXT, "+ LATITUDE+" TEXT,"+LONGITUDE+" TEXT)";
				String CREATE_TABLE_MYORDER="CREATE TABLE "
						+ TABLE_ORDR + "(" + ORDERID + " INTEGER PRIMARY KEY, "
						+ ORDERDATE + " TEXT, " + ORDERINFO + " TEXT," + ORDERPRICE + " TEXT, "+ORDERTYPE+" TEXT, "+ORDERSTATUS+" TEXT)";
				db.execSQL(CREATE_TABLE_MYORDER);
				db.execSQL(CREATE_TABLE_USERS);
				db.execSQL(CREATE_USER_LOCATION_TABLE);


			} catch (IOException e) {
        		throw new Error("Error copying database");
        	}
    	}

    }
 
   
   
    private boolean checkDataBase(){
 
    	File dbFile = new File(DB_PATH + DB_NAME);

    	return dbFile.exists();
    	
    }
 
    
    private void copyDataBase() throws IOException{
    	
    	InputStream myInput = context.getAssets().open(DB_NAME);
 
    	String outFileName = DB_PATH + DB_NAME;
 
    	OutputStream myOutput = new FileOutputStream(outFileName);
    	
    	byte[] buffer = new byte[1024];
    	int length;
    	while ((length = myInput.read(buffer))>0){
    		myOutput.write(buffer, 0, length);
    	}
 
    	myOutput.flush();
    	myOutput.close();
    	myInput.close();
 
    }
 
    public void openDataBase() throws SQLException{
    	String myPath = DB_PATH + DB_NAME;
    	db = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READWRITE);
    }
 
    @Override
	public void close() {
    	db.close();
	}
 
	@Override
	public void onCreate(SQLiteDatabase db) {

	}
 
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

		db.execSQL("DROP TABLE IF EXISTS " + TABLE_ADDR);
		String CREATE_TABLE_USERS = "CREATE TABLE "
				+ TABLE_ADDR  + "(" + ID + " INTEGER PRIMARY KEY, "
				+ NAME + " TEXT, " + ADDR + " TEXT," + CITY + " TEXT ," + SHOPCODE + " TEXT , "
				+ PHONE + " TEXT, " + EMAIL + " TEXT, "+ LATITUDE+" TEXT,"+LONGITUDE+" TEXT)";
		db.execSQL(CREATE_TABLE_USERS);
		db.execSQL("DROP TABLE IF EXISTS " + LOCATION_TABLE);
		db.execSQL(CREATE_USER_LOCATION_TABLE);

		db.execSQL("DROP TABLE IF EXISTS " + TABLE_ORDR);
		String CREATE_TABLE_MYORDER="CREATE TABLE "
				+ TABLE_ORDR + "(" + ORDERID + " INTEGER PRIMARY KEY, "
				+ ORDERDATE + " TEXT, " + ORDERINFO + " TEXT," + ORDERPRICE + " TEXT)";
		db.execSQL(CREATE_TABLE_MYORDER);

	}
	
	/** this code is used to get all data from database */
 	public ArrayList<ArrayList<Object>> getAllData(){
		ArrayList<ArrayList<Object>> dataArrays = new ArrayList<ArrayList<Object>>();
 
		Cursor cursor = null;
 
			try{
				cursor = db.query(
						TABLE_NAME,
						new String[]{ID, MENU_NAME, QUANTITY, TOTAL_PRICE},
						null,null, null, null, null);
				cursor.moveToFirst();
	
				if (!cursor.isAfterLast()){
					do{
						ArrayList<Object> dataList = new ArrayList<Object>();
	 
						dataList.add(cursor.getLong(0));
						dataList.add(cursor.getString(1));
						dataList.add(cursor.getString(2));
						dataList.add(cursor.getString(3));
	 
						dataArrays.add(dataList);
					}
				
					while (cursor.moveToNext());
				}
				cursor.close();
			}catch (SQLException e){
				Log.e("DB Error", e.toString());
				e.printStackTrace();
			}
		
		return dataArrays;
	}
 	
 	/** this code is used to get all data from database */
 	public boolean isDataExist(long id){
 		boolean exist = false;
		
		Cursor cursor = null;
 
			try{
				cursor = db.query(
						TABLE_NAME,
						new String[]{ID},
						ID +"="+id,
						null, null, null, null);
				if(cursor.getCount() > 0){
					exist = true;
				}

				cursor.close();
			}catch (SQLException e){
				Log.e("DB Error", e.toString());
				e.printStackTrace();
			}
		
		return exist;
	}
 	
 	/** this code is used to get all data from database */
 	public boolean isPreviousDataExist(){
 		boolean exist = false;
		
		Cursor cursor = null;
 
			try{
				cursor = db.query(
						TABLE_NAME,
						new String[]{ID},
						null,null, null, null, null);
				if(cursor.getCount() > 0){
					exist = true;
				}

				cursor.close();
			}catch (SQLException e){
				Log.e("DB Error", e.toString());
				e.printStackTrace();
			}
		
		return exist;
	}

 	public void addData(long id, String menu_name, int quantity, double total_price){
		// this is a key value pair holder used by android's SQLite functions
		ContentValues values = new ContentValues();
		values.put(ID, id);
		values.put(MENU_NAME, menu_name);
		values.put(QUANTITY, quantity);
		values.put(TOTAL_PRICE, total_price);
 
		// ask the database object to insert the new data 
		try{db.insert(TABLE_NAME, null, values);}
		catch(Exception e)
		{
			Log.e("DB ERROR", e.toString());
			e.printStackTrace();
		}
	}

	public void addMyorders(String orderDate,String orderInfo,String orderPrice,String ordertype,String orderStatus)
	{
		ContentValues values = new ContentValues();
		values.put(ORDERDATE,orderDate);
		values.put(ORDERINFO,orderInfo);
		values.put(ORDERPRICE,orderPrice);
		values.put(ORDERTYPE,ordertype);
		values.put(ORDERSTATUS,orderStatus);

		try{db.insert(TABLE_ORDR, null, values);}
		catch(Exception e)
		{
			Log.e("DB ERROR", e.toString());
			e.printStackTrace();
		}
	}

	public ArrayList<String> getMyorders(){
		ArrayList<String> dataArrays = new ArrayList<String>();

		Cursor cursor = null;

		try{


			cursor = db.query(
					TABLE_ORDR,
					new String[]{ID, ORDERDATE, ORDERINFO, ORDERPRICE},
					null,null, null, null, null);
			cursor.moveToFirst();

			if (!cursor.isAfterLast()){
				do{

					dataArrays.add(String.valueOf(cursor.getLong(0)));
					dataArrays.add(cursor.getString(1));
					dataArrays.add(cursor.getString(2));
					dataArrays.add(cursor.getString(3));

				}

				while (cursor.moveToNext());
			}
			cursor.close();
		}catch (SQLException e){
			Log.e("DB Error", e.toString());
			e.printStackTrace();
		}

		return dataArrays;
	}

	public ArrayList<Orders>  getAllOrders(){

		ArrayList<Orders> data = new ArrayList<Orders>();


		// Select All Query

		String selectQuery = "SELECT " + ORDERID+","+ ORDERDATE+","+ ORDERINFO+","+ORDERPRICE+" FROM " + TABLE_ORDR +" ORDER BY orderid DESC";

		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		if (cursor.moveToFirst()) {
			do {
				data.add(new Orders(cursor.getString(0),cursor.getString(1),cursor.getString(2)));
				Log.d("Orders",cursor.getString(0)+","+cursor.getString(1)+","+cursor.getString(2));
			} while (cursor.moveToNext());
		}
		cursor.close();

		return data;
	}

	public ArrayList<AddressPojo> getAllAddress()
	{
		ArrayList<AddressPojo> addrData=new ArrayList<AddressPojo>();
		String select_num="SELECT "+NAME+","+ADDR+","+CITY+","+SHOPCODE+","+PHONE+","+EMAIL+","+LATITUDE+","+LONGITUDE+" FROM "+ TABLE_ADDR  ;
		SQLiteDatabase db=this.getReadableDatabase();
		Cursor cursor=db.rawQuery(select_num,null);
		if(cursor.moveToFirst())
		{
			do
			{
				AddressPojo ap=new AddressPojo();
				ap.setTxtAddress(cursor.getString(1));
				ap.setTxtBNFN(cursor.getString(2));
				ap.setTxtPhone(cursor.getString(4));
				ap.setTxtLat(cursor.getString(6));
				ap.setTxtLon(cursor.getString(7));

				addrData.add(ap);
			}while (cursor.moveToNext());
		}

		return addrData;
	}

	public void addAddr(String name,String address,String city,String shopcode,String phone,String email,String latitude,String longitude)
	{
		ContentValues values = new ContentValues();
		values.put(NAME,name);
		values.put(ADDR,address);
		values.put(CITY,city);
		values.put(SHOPCODE,shopcode);
		values.put(PHONE,phone);
		values.put(EMAIL,email);
		values.put(LATITUDE,latitude);
		values.put(LONGITUDE,longitude);
		try{db.insert(TABLE_ADDR, null, values);}
		catch(Exception e)
		{
			Log.e("DB ERROR", e.toString());
			e.printStackTrace();
		}
	}
	public int addrCount()
	{
		int Count=0;
		String selectQuery = "SELECT Count(*) FROM " + TABLE_ADDR ;
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);
		if (cursor.moveToFirst()) {
			do {
				Count=Integer.parseInt(cursor.getString(0));
			} while (cursor.moveToNext());
		}
		return Count;
	}

	public String getAddress()
	{
		String data="";
		String select_num="SELECT "+NAME+","+ADDR+","+CITY+","+SHOPCODE+","+PHONE+","+EMAIL+" FROM "+ TABLE_ADDR  ;
		SQLiteDatabase db=this.getReadableDatabase();
		Cursor cursor=db.rawQuery(select_num,null);
		if(cursor.moveToFirst())
		{
			do
			{
				data =cursor.getString(0)+","+cursor.getString(1)+","+cursor.getString(2)+","+cursor.getString(3)+","+cursor.getString(4)+","+cursor.getString(5);

			}while (cursor.moveToNext());
		}
		cursor.close();
		db.close();
		return data;

	}
 	
 	public void deleteData(long id){
		// ask the database manager to delete the row of given id
		try {db.delete(TABLE_NAME, ID + "=" + id, null);}
		catch (Exception e)
		{
			Log.e("DB ERROR", e.toString());
			e.printStackTrace();
		}
	}
 	
 	public void deleteAllData(){
		// ask the database manager to delete the row of given id
		try {db.delete(TABLE_NAME, null, null);}
		catch (Exception e)
		{
			Log.e("DB ERROR", e.toString());
			e.printStackTrace();
		}
	}
 	
 	public void updateData(long id, int quantity, double total_price){
		// this is a key value pair holder used by android's SQLite functions
		ContentValues values = new ContentValues();
		values.put(QUANTITY, quantity);
		values.put(TOTAL_PRICE, total_price);
 
		// ask the database object to update the database row of given rowID
		try {db.update(TABLE_NAME, values, ID + "=" + id, null);}
		catch (Exception e)
		{
			Log.e("DB Error", e.toString());
			e.printStackTrace();
		}
	}

	public void insertLocation(String Name,String Latitude,String Longitude)
	{
		SQLiteDatabase db=this.getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put(NAME_LOCATION_COLUMN,Name);
		values.put(LATITUDE_COLOMN, Latitude);
		values.put(LONGITUDE_COLOMN,Longitude);
		db.insert(DBHelper.LOCATION_TABLE, null, values);
	}
	public long updateLocation(String Name,String Latitude,String Longitude)
	{
		String WHERE_ID_EQUALS = DBHelper.ID_COLUMN
				+ " !=?";
		SQLiteDatabase db=this.getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put(NAME_LOCATION_COLUMN,Name);
		values.put(LATITUDE_COLOMN, Latitude);
		values.put(LONGITUDE_COLOMN,Longitude);
		long result = db.update(DBHelper.LOCATION_TABLE, values,
				WHERE_ID_EQUALS,
				null);
		return result;
	}
	public String getLocationName()
	{
		SQLiteDatabase db=this.getWritableDatabase();
		String selectQuery = "SELECT "+NAME_LOCATION_COLUMN+" FROM " + LOCATION_TABLE;

		Cursor cursor = db.rawQuery(selectQuery, null);
		String Location="";
		if (cursor.moveToFirst()) {
			do {
				Location=cursor.getString(0).toString();
			} while (cursor.moveToNext());
		}
		return Location;
	}
	public String getLocationLatitude()
	{
		SQLiteDatabase db=this.getWritableDatabase();
		String selectQuery = "SELECT "+LATITUDE_COLOMN+" FROM " + LOCATION_TABLE;

		Cursor cursor = db.rawQuery(selectQuery, null);
		String Location="";
		if (cursor.moveToFirst()) {
			do {
				Location=cursor.getString(0).toString();
			} while (cursor.moveToNext());
		}
		return Location;
	}

	public String getLocationLongitude()
	{
		SQLiteDatabase db=this.getWritableDatabase();
		String selectQuery = "SELECT "+LONGITUDE_COLOMN+" FROM " + LOCATION_TABLE;

		Cursor cursor = db.rawQuery(selectQuery, null);
		String Location="";
		if (cursor.moveToFirst()) {
			do {
				Location=cursor.getString(0).toString();
			} while (cursor.moveToNext());
		}
		return Location;
	}
	public void deleteLocations()
	{
		SQLiteDatabase db=this.getWritableDatabase();
		String sql="DELETE FROM " + LOCATION_TABLE ;
		db.execSQL(sql);
	}
	// checks the value already there int he table
	public boolean checkAlreadyExists(Context context, String tablename,
									  String columnname, String columnvalue) {
		boolean value = false;
		String countQuery = "SELECT  * FROM " + tablename + " WHERE "
				+ columnname + "='" + columnvalue + "'";
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.rawQuery(countQuery, null);
		int count = cursor.getCount();
		if (count > 0)
			value = true;
		cursor.close();
		db.close();
		// return count
		return value;
	}

	public static int getCount(Context context, final String tableName) {
		DBHelper dbhelper = new DBHelper(context);
		SQLiteDatabase db = dbhelper.getReadableDatabase();
		int cnt = 0;
		Cursor cursor = db.rawQuery("SELECT COUNT(*) FROM " + tableName, null);
		cursor.moveToFirst();
		cnt = Integer.parseInt(cursor.getString(0));
		cursor.close();
		db.close();
		return cnt;

	}

	public List<String[]> getTableDataByValue(Context context,
											  String TableName, String columname, String columnValue) {
		DBHelper dbhelper = new DBHelper(context);
		SQLiteDatabase db = dbhelper.getReadableDatabase();
		Cursor cur = db.rawQuery("SELECT * FROM " + TableName + " WHERE "
				+ columname + "='" + columnValue + "'", null);
		List<String[]> list = new ArrayList<String[]>();
		// System.out.println("cur:" + cur.getCount());
		if (cur.getCount() > 0)
			list = cursortoListArr(cur);
		cur.close();
		db.close();
		dbhelper.close();

		return list;
	}

	public List<String[]> cursortoListArr(Cursor c) {
		List<String[]> rowList = new ArrayList<String[]>();
		while (c.moveToNext()) {
			String[] arr = new String[c.getColumnCount()];
			for (int i = 0; i < c.getColumnCount(); i++) {
				arr[i] = c.getString(i);
			}
			rowList.add(arr);
		}
		c.close();
		return rowList;
	}

	public static boolean updateRowData(Context context,
										final String tablename, String[] columnNames,
										String[] columnValues, String[] whereColumn, String[] whereValue) {
		System.out.println("In Update...");
		DBHelper dbhelper = new DBHelper(context);
		SQLiteDatabase db = dbhelper.getReadableDatabase();
		ContentValues cv = new ContentValues();
		for (int i = 0; i < columnNames.length; i++) {
			cv.put(columnNames[i], columnValues[i]);
		}
		boolean flag = false;
		if (whereColumn != null) {
			String whereClause = "";
			for (int k = 0; k < whereColumn.length; k++) {
				whereClause = whereClause + whereColumn[k] + "='"
						+ whereValue[k] + "'" + " AND ";
			}
			whereClause = whereClause.substring(0, whereClause.length() - 5);
			System.out.println("whereclause......" + whereClause);
			flag = db.update(tablename, cv, whereClause, null) > 0;
		} else {
			flag = db.update(tablename, cv, null, null) > 0;
		}

		db.close();
		System.out.println("flag.................." + flag);
		return flag;
	}

	public static long insertintoTable(Context context, final String tableName,
									   String[] colNames, String[] values) {

		DBHelper dbhelper = new DBHelper(context);
		SQLiteDatabase db = dbhelper.getReadableDatabase();
		ContentValues cv = new ContentValues();
		for (int i = 0; i < colNames.length; i++) {
			cv.put(colNames[i], values[i]);
		}
		long cnt = db.insert(tableName, null, cv);
		db.close();
		return cnt;
	}

	public long insertintoTable1(String tableName, String[] colNames,
								 String[] colVals) {

		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues cv = new ContentValues();
		for (int i = 0; i < colNames.length; i++) {
			cv.put(colNames[i], colVals[i]);
		}
		long result = db.insert(tableName, null, cv);
		db.close();
		return result;
	}

	public boolean updateByQuery(String tablename, String[] columnNames,
								 String[] columnValues, String Condition) {

		SQLiteDatabase db = this.getReadableDatabase();
		ContentValues cv = new ContentValues();
		for (int i = 0; i < columnNames.length; i++) {
			cv.put(columnNames[i], columnValues[i]);
		}
		boolean flag = db.update(tablename, cv, Condition, null) > 0;
		db.close();
		return flag;

	}
	public boolean updateByQuery(String tablename, String columnNames,
								 String columnValues, String Condition) {

		SQLiteDatabase db = this.getReadableDatabase();
		ContentValues cv = new ContentValues();
		cv.put(columnNames, columnValues);

		boolean flag = db.update(tablename, cv, Condition, null) > 0;
		db.close();
		return flag;

	}

	public List<List<String>> getDataByQuery(String query) {
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cur = db.rawQuery(query, null);
		List<List<String>> list = new ArrayList<List<String>>();
		if (cur.getCount() > 0)
			list = cursorToListArr(cur);
		cur.close();
		db.close();
		return list;
	}
	public List<List<String>> cursorToListArr(Cursor c) {
		List<List<String>> rowList = new ArrayList<List<String>>();
		while (c.moveToNext()) {
			List<String> arr = new ArrayList<String>();
			for (int i = 0; i < c.getColumnCount(); i++) {
				arr.add(c.getString(i));
			}
			rowList.add(arr);
		}
		c.close();
		return rowList;
	}



}