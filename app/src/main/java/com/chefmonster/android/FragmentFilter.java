package com.chefmonster.android;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import de.keyboardsurfer.android.widget.crouton.Crouton;
import de.keyboardsurfer.android.widget.crouton.Style;

/**
 * Created by tskva on 7/13/2016.
 */
public class FragmentFilter extends Fragment {

    Button applyFilter,clearFilter;
    RadioGroup rgb;
    RadioButton vegrb,nonvegrb;
    private boolean mFilterAlreadyApplied;
    private static  final String filtersBool="filtersbool";
    private static  final String filters="filters";
    TextView appliedFilter;

    android.app.Fragment fragment2 = null;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_filter, container, false);
        applyFilter=(Button)v.findViewById(R.id.btnApplyFilters);
        clearFilter=(Button)v.findViewById(R.id.btnClearFilters);
        appliedFilter=(TextView)v.findViewById(R.id.txtAppliedFilters);

        final SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());

        if(didFilterAlreadyApplied())
        {
            mFilterAlreadyApplied = true;
            sharedPreferences.edit().putBoolean(filtersBool, mFilterAlreadyApplied).apply();
          //  sharedPreferences.edit().putString(filters, appliedFilter.getText().toString()).apply();
            appliedFilter.setText(sharedPreferences.getString(filters,null));
        }
        applyFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                 if(appliedFilter.getText().toString().equals(""))
                 {
                     Crouton.makeText(getActivity(), "No Filter's Selected", Style.ALERT).show();
                 }
                else
                 {
                     mFilterAlreadyApplied = true;
                     sharedPreferences.edit().putBoolean(filtersBool, mFilterAlreadyApplied).apply();
                     sharedPreferences.edit().putString(filters, appliedFilter.getText().toString()).apply();
                     appliedFilter.setText(sharedPreferences.getString(filters,null));

                     fragment2 = new ActivityMenuList();
                     FragmentTransaction ft=getFragmentManager().beginTransaction();
                     ft.replace(R.id.filterLayout,fragment2);
                     ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
                     ft.addToBackStack(null);
                     ft.commit();
                 }


            }
        });
        clearFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                appliedFilter.setText("");
                mFilterAlreadyApplied = false;
                sharedPreferences.edit().putBoolean(filtersBool, mFilterAlreadyApplied).apply();
                sharedPreferences.edit().remove(filters).commit();
            }
        });
        rgb=(RadioGroup) v.findViewById(R.id.rbgftype);
        rgb.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {

                switch(i)
                {

                    case R.id.filterRbVeg:

                        appliedFilter.setText("Veg");



                        break;
                    case R.id.filterRbNonVeg:

                        appliedFilter.setText("Non-Veg");


                        break;

                }
            }
        });
        return v;
    }

    private boolean didFilterAlreadyApplied() {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        mFilterAlreadyApplied = sharedPreferences.getBoolean(filtersBool, false);
        return mFilterAlreadyApplied;
    }
}
