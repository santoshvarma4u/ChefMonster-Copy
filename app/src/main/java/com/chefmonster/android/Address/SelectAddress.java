package com.chefmonster.android.Address;

import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.chefmonster.android.DBHelper;
import com.chefmonster.android.R;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.model.LatLng;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class SelectAddress extends Activity {


    private List<AddressPojo> addressList = new ArrayList<>();
    private RecyclerView recyclerView;
    private AddressAdapater mAdapter;
    Geocoder geocoder;
    int PLACE_PICKER_REQUEST = 1789;

    List<Address> addresses;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_address);
//        getActionBar().setDisplayHomeAsUpEnabled(true);


        geocoder = new Geocoder(this, Locale.getDefault());

        recyclerView = (RecyclerView) findViewById(R.id.address_view);

        //fab button


        loadAddress();
    }
    public void loadAddress()
    {
        DBHelper db=new DBHelper(getApplicationContext());
        if(db.getCount(getApplicationContext(),db.TABLE_ADDR) > 0)
        {
            addressList.clear();
            addressList=db.getAllAddress();

            mAdapter = new AddressAdapater(addressList,getApplicationContext());
            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
            recyclerView.setLayoutManager(mLayoutManager);
            recyclerView.setItemAnimator(new DefaultItemAnimator());
            recyclerView.setAdapter(mAdapter);

            mAdapter.notifyDataSetChanged();
        }

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == PLACE_PICKER_REQUEST) {
            if (resultCode == RESULT_OK) {
                Place place = PlacePicker.getPlace(data, this);
              /*  Log.i("Place",place.getAddress().toString());
                Log.i("Place",place.getLocale().toString());*/

                final LatLng ll = place.getLatLng();

                String cusLat = String.valueOf(ll.latitude);
                String cusLon = String.valueOf(ll.longitude);
                try {
                    addresses = geocoder.getFromLocation(ll.latitude, ll.longitude, 1);
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
//                String API="AIzaSyAMI359r57a3S546PL5SIbe3BBl8YCglso";
//                String DDUrl="https://maps.googleapis.com/maps/api/distancematrix/json?units=metric&origins="+cusLat+","+cusLon+"&destinations="+chefLat+"%2C"+chefLon+"%7C&key="+API;

                String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                String city = addresses.get(0).getLocality();
                String state = addresses.get(0).getAdminArea();
                String country = addresses.get(0).getCountryName();
                String postalCode = addresses.get(0).getPostalCode();
                String knownName = addresses.get(0).getFeatureName();
                final String adrString = address + " ," + city + " ," + state + " ," + country;

                  /*  Log.i("address",address);
                    Log.i("city",city);
                    Log.i("state",state);
                    Log.i("country",country);
                    Log.i("postalCode",postalCode);
                    Log.i("knownName",knownName);*/
                View adv = getLayoutInflater().inflate(R.layout.addressdialog, null, false);
                final EditText firstname=(EditText) adv.findViewById(R.id.et_name_addr);
                final EditText CompleteAddress = (EditText) adv.findViewById(R.id.fullAddr);
                final EditText bnfn = (EditText) adv.findViewById(R.id.bnfn);
                final EditText adrphone = (EditText) adv.findViewById(R.id.adrphone);
                final EditText adrEmail = (EditText) adv.findViewById(R.id.adremail);
                CompleteAddress.setText(adrString);
                boolean wrapInScrollView = true;
                new MaterialDialog.Builder(this)
                        .autoDismiss(true)
                        .title("Add Address")
                        .customView(adv, wrapInScrollView)
                        .positiveText("Confirm Address")
                        .onPositive(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {

                                DBHelper db=new DBHelper(getApplicationContext());
                                db.insertintoTable1(db.TABLE_ADDR,new String[]{db.NAME,db.ADDR,db.CITY,db.PHONE,db.EMAIL,db.LATITUDE,db.LONGITUDE
                                },new String[]{firstname.getText().toString(),CompleteAddress.getText().toString(),bnfn.getText().toString(),
                                        adrphone.getText().toString(),adrEmail.getText().toString(),String.valueOf(ll.latitude),String.valueOf(ll.longitude)});
                                Toast.makeText(getApplicationContext(), "Address addded", Toast.LENGTH_SHORT).show();
                                loadAddress();
                                mAdapter.notifyDataSetChanged();
                                dialog.dismiss();

                            }
                        })
                        .negativeText("Change Address")
                        .onNegative(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {

                                PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
                                try {
                                    startActivityForResult(builder.build(SelectAddress.this), PLACE_PICKER_REQUEST);
                                } catch (GooglePlayServicesRepairableException e) {
                                    // TODO Auto-generated catch block
                                    e.printStackTrace();
                                } catch (GooglePlayServicesNotAvailableException e) {
                                    // TODO Auto-generated catch block
                                    e.printStackTrace();
                                }
                            }
                        })
                        .show();
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.newaddr, menu);
        MenuItem mi=menu.findItem(R.id.ic_menu);
        mi.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {

                PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
                try {
                    startActivityForResult(builder.build(SelectAddress.this), PLACE_PICKER_REQUEST);
                } catch (GooglePlayServicesRepairableException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                } catch (GooglePlayServicesNotAvailableException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                return false;
            }
        });

        return super.onCreateOptionsMenu(menu);
    }
}
