package com.chefmonster.android.Address;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.chefmonster.android.ActivityMenuList;
import com.chefmonster.android.MainActivity;
import com.chefmonster.android.R;
import com.chefmonster.android.model.Address;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by tskva on 7/19/2016.
 */
public class AddressAdapater extends RecyclerView.Adapter<AddressAdapater.ViewHolder> {

    public List<AddressPojo> addressList;
    Context ctx;

    public AddressAdapater(List<AddressPojo> addressList,Context context) {
        this.addressList=addressList;
        this.ctx=context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View addressView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.addresslayout, parent, false);

        return new ViewHolder(addressView);

    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        AddressPojo addr = addressList.get(position);
        //holder.code.setText(" (" +  addr.getLocation() + ")");
        holder.Address.setText(addr.getTxtBNFN()+" ,"+addr.getTxtAddress());
        holder.selected.setChecked(false);
        //holder.Address.setTag(addr);
    }

    @Override
    public int getItemCount() {
        return addressList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public ImageView Location;
        public  TextView Address;
        public CheckBox selected;

        final SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(ctx);

        public ViewHolder(View itemView) {
            super(itemView);
            Location = (ImageView) itemView.findViewById(R.id.imgLocType);;
            Address=(TextView) itemView.findViewById(R.id.txt_lv_address);
            selected=(CheckBox) itemView.findViewById(R.id.cb_lv_addr);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {

            sharedPreferences.edit().putString("cAddress",  addressList.get(getAdapterPosition()).getTxtAddress()).apply();
            sharedPreferences.edit().putString("cBNFN",  addressList.get(getAdapterPosition()).getTxtBNFN()).apply();
            sharedPreferences.edit().putString("cPhone",  addressList.get(getAdapterPosition()).getTxtPhone()).apply();
            sharedPreferences.edit().putString("cLatitude",  addressList.get(getAdapterPosition()).getTxtLat()).apply();
            sharedPreferences.edit().putString("cLongitude",  addressList.get(getAdapterPosition()).getTxtLon()).apply();
            sharedPreferences.edit().putBoolean("Location", true).apply();

            Intent intent = new Intent(ctx,MainActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            ctx.startActivity(intent);

        }
    }

}
