package com.chefmonster.android.Address;

/**
 * Created by tskva on 7/22/2016.
 */
public class AddressPojo {


    String txtAddress,txtBNFN,txtPhone,txtLat,txtLon;
    Boolean selectedAddr;
    public AddressPojo()
    {

       /* this.txtOrderID=txtOrderID;
        this.txtOrderDate=txtOrderDate;
        this.txtOrderInfo=txtOrderInfo;*/
    }

    public Boolean getSelectedAddr() {
        return selectedAddr;
    }

    public void setSelectedAddr(Boolean selectedAddr) {
        this.selectedAddr = selectedAddr;
    }

    public void setTxtAddress(String txtAddress) {
        this.txtAddress = txtAddress;
    }

    public String getTxtAddress() {
        return txtAddress;
    }

    public String getTxtBNFN() {
        return txtBNFN;
    }

    public void setTxtBNFN(String txtBNFN) {
        this.txtBNFN = txtBNFN;
    }
    public String getTxtLat() {
        return txtLat;
    }

    public void setTxtLat(String txtLat) {
        this.txtLat = txtLat;
    }

    public String getTxtLon() {
        return txtLon;
    }

    public void setTxtLon(String txtLon) {
        this.txtLon = txtLon;
    }

    public String getTxtPhone() {
        return txtPhone;
    }

    public void setTxtPhone(String txtPhone) {
        this.txtPhone = txtPhone;
    }
}
