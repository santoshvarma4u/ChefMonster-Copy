package com.chefmonster.android;

/**
 * Created by kalya on 25-03-2016.
 */
public class Orders {

    public String getTxtOrderID() {
        return txtOrderID;
    }

    public void setTxtOrderID(String txtOrderID) {
        this.txtOrderID = txtOrderID;
    }

    public String getTxtOrderDate() {
        return txtOrderDate;
    }

    public void setTxtOrderDate(String txtOrderDate) {
        this.txtOrderDate = txtOrderDate;
    }

    public String getTxtOrderInfo() {
        return txtOrderInfo;
    }

    public void setTxtOrderInfo(String txtOrderInfo) {
        this.txtOrderInfo = txtOrderInfo;
    }

    String txtOrderID,txtOrderDate,txtOrderPrice,txtOrderInfo;

    public Orders(String txtOrderID, String txtOrderDate, String txtOrderInfo)
    {
        this.txtOrderID=txtOrderID;
        this.txtOrderDate=txtOrderDate;
        this.txtOrderInfo=txtOrderInfo;
    }

}
