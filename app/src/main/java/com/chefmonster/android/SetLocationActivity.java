package com.chefmonster.android;


import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.preference.PreferenceManager;
import android.os.Bundle;
import android.util.Log;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocompleteFragment;
import com.google.android.gms.location.places.ui.PlaceSelectionListener;

import java.util.Set;



public class SetLocationActivity extends Activity {

    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;
    private long UPDATE_INTERVAL = 2 * 1000;
    private long FASTEST_INTERVAL = 2000;


    private boolean mUserLocationSaved = false;
    private static  final String LocationStage="Location";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_set_location);
        final SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        PlaceAutocompleteFragment autocompleteFragment = (PlaceAutocompleteFragment)
                getFragmentManager().findFragmentById(R.id.place_autocomplete_fragment);

        autocompleteFragment.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(Place place) {
                // TODO: Get info about the selected place.
                Log.i("Places", "Place: " + place.getName());
                double latitude=place.getLatLng().latitude;
                double longitude=place.getLatLng().longitude;

                DBHelper dbh=new DBHelper(getApplicationContext());


                if(!didUserLocationSaved())
                {
                    dbh.insertLocation(place.getName().toString(),String.valueOf(latitude),String.valueOf(longitude));
                    mUserLocationSaved = true;
                    sharedPreferences.edit().putBoolean(LocationStage, mUserLocationSaved).apply();
                    Intent intent=new Intent(SetLocationActivity.this,MainActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    // Add new Flag to start new Activity
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    overridePendingTransition(R.anim.open_next, R.anim.close_next);
                    finish();
                }
                else
                {
                    dbh.deleteLocations();
                    dbh.insertLocation(place.getName().toString(),String.valueOf(latitude),String.valueOf(longitude));
                    sharedPreferences.edit().putBoolean(LocationStage, mUserLocationSaved).apply();
                    Intent intent=new Intent(SetLocationActivity.this,MainActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    // Add new Flag to start new Activity
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    overridePendingTransition(R.anim.open_next, R.anim.close_next);
                    finish();
                }
            }
            @Override
            public void onError(Status status) {
                // TODO: Handle the error.
                Log.i("Places", "An error occurred: " + status);
            }
        });



    }

    private boolean didUserLocationSaved() {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        mUserLocationSaved = sharedPreferences.getBoolean(LocationStage, false);
        return mUserLocationSaved;
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.open_next, R.anim.close_next);
    }
}
