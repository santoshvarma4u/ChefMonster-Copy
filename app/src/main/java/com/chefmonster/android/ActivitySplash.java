package com.chefmonster.android;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.view.Window;
import android.widget.Toast;


import java.util.ArrayList;

public class ActivitySplash extends Activity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //menyembunyikan title bar di layar acitivy
        getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        //membuat activity menjadi fullscreen
//        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, 
//        WindowManager.LayoutParams.FLAG_FULLSCREEN);




        /** Sets a layout for this activity */
        setContentView(R.layout.splash);

        /** Creates a count down timer, which will be expired after 5000 milliseconds */
        new CountDownTimer(5000,1000) {

        	/** This method will be invoked on finishing or expiring the timer */
			@Override
			public void onFinish() {
				/** Creates an intent to start new activity */
				if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

					if(ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE)!= PackageManager.PERMISSION_GRANTED)
					{
						ActivityCompat.requestPermissions(ActivitySplash.this, new String[]{Manifest.permission.INTERNET,Manifest.permission.CALL_PHONE,Manifest.permission.ACCESS_NETWORK_STATE,Manifest.permission.GET_ACCOUNTS,Manifest.permission.WRITE_EXTERNAL_STORAGE,Manifest.permission.ACCESS_FINE_LOCATION,Manifest.permission.READ_CONTACTS,Manifest.permission.READ_PHONE_STATE}, 1257);
					}
					else
					{
						Intent intent = new Intent(getBaseContext(), MainActivity.class);
						startActivity(intent);
						finish();
					}
				}
				else
				{
					Intent intent = new Intent(getBaseContext(), MainActivity.class);
					startActivity(intent);
					finish();
				}
			}

			/** This method will be invoked in every 1000 milli seconds until
			* this timer is expired.Because we specified 1000 as tick time
			* while creating this CountDownTimer
			*/
			@Override
			public void onTick(long millisUntilFinished) {

			}
		}.start();

    }

	@Override
	public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
		switch (requestCode) {
			case 1257:
				if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

					Intent intent = new Intent(getBaseContext(), MainActivity.class);
					startActivity(intent);
					finish();
				}
				else
				{
					Toast.makeText(getApplicationContext(),"Please Allow Permissions to access the library",Toast.LENGTH_SHORT).show();
				}
				break;
		}
	}

}